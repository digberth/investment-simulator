<%@ include file="init.jsp" %>

<%-- <script src="<%=request.getContextPath()%>/js/simulator.js" type="text/javascript"></script> --%>
<portlet:resourceURL var="getDiversifiedURL" id="get-diversified" >
</portlet:resourceURL>
<%
	String pen = (String)renderRequest.getPortletSession().getAttribute("LIFERAY_SHARED_penCompleted", PortletSession.APPLICATION_SCOPE);
	String tri = (String)renderRequest.getPortletSession().getAttribute("LIFERAY_SHARED_tribCompleted", PortletSession.APPLICATION_SCOPE);
	String fin = (String)renderRequest.getPortletSession().getAttribute("LIFERAY_SHARED_finCompleted", PortletSession.APPLICATION_SCOPE);
	
	int count = 0;
	if(pen != null && pen.equals(active))count++;
	if(tri != null && tri.equals(active))count++;
	if(fin != null && fin.equals(active))count++;

	String nombre = (String)renderRequest.getPortletSession().getAttribute("LIFERAY_SHARED_personName", PortletSession.APPLICATION_SCOPE);
	nombre = (nombre == null || nombre.isEmpty()) ? "Hola" : nombre;
%>

<!-- Bienvenida -->
<section class="main_section simulador blue-bg">
    <div class="titles-banner"> <span class="icons-chart"></span>
        <h1 class="titleMob inv semi">Simulemos <br>tu Inversi�n</h1> </div>
    <div class="center-complement">
        <div class="col-xs-12 col-sm-7">
            <div class="luis3d mobile">
                <figure>                   
                    <img  src="<%= request.getContextPath()%>/img/rostro_luis1.png" />                    
                </figure>
            </div>
        </div>
        <div class="col-xs-12 col-sm-5">
            <div class="box-desk">
                <form action="#" class="userName">
                    <div class="box-info">
                        <p class="text-md inv center-mob">Con nuestro simulador de inversiones, proyectaremos  el crecimiento de tu capital a trav�s de nuestras Alternativas de inversi�n.<br><br>Recuerda, antes de tomar una decisi�n, es importante definir algunos aspectos claves: <br><br></p>                        
                        <ul>                            
                            <li class="text-md inv">1 Objetivo</li>          
              <li class="text-md inv">2 Tiempo de inversi�n</li>      
              <li class="text-md inv">3 Nivel de tolerancia de riesgo</li>                    
                        </ul>       
                        <button class="boton-primario btn-center remove-stripe">Comencemos <span class="icon-siguiente"></span></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<!-- Simulaci�n de datos -->
<section class="main_section simulador-plan no-section" id="hides">
   <div class="titles-banner user">
      <span class="icons-chart"></span>
      <h1 class="titleMob inv semi"><%=nombre %></h1>
   </div>
   <div class="center-complement">
      <div class="col-xs-12 no-section">
         <div class="box-desk">
            <div class="box-info">                       
               <div class="slider">               
                  <div class="itemCarusel">
                     <div class="answer-banner">
                        <p class="text-md"><span class="semi">1.</span> Empecemos a construir tu plan de inversi�n</p>
                     </div>
                     <div class="form-value">
                        <form id="obj-form" class="forms-elm">
                           <div class="form-group col-xs-12 col-sm-4 center-simulador">
                              <label for="inversion21" class="title-label">� Para qu� deseas invertir?</label>
                              <select class="selectpicker btn-white claro required" data-width="100%" title="Selecciona tu objetivo" id="inversion21">
                                 <option value="opt1">Vivienda</option>
                                 <option value="opt2">Educaci�n</option>
                                 <option value="opt3">Complementar mi pensi�n</option>
                                 <option value="opt4">Otros objetivos de ahorro</option>
                              </select>
                           </div>
                        </form>
                     </div>
                     <div class="form-value" id="obj-estandar">
                        <form  id="data-form"  class="forms-elm">
                           <div class="form-group col-sm-4 ">
                              <label for="inversion2" class="title-label">G�nero</label>
                              <div class="input-group addons-porvenir">
                                 <span class="input-group-addon"><i class="icons-gender"></i></span>
                                 <select class="selectpicker btn-white claro " title="" data-width="100%" id="gender">
                                    <option>Hombre</option>
                                    <option>Mujer</option>
                                 </select>
                              </div>
                           </div>
                           <div class="form-group col-sm-4 dates">
                              <label for="inversion3" class="title-label">Fecha de nacimiento</label>
                              <div class="input-group addons-porvenir">
                                 <div id="datepicker" class="datepicker input-group date" data-date-format="dd/mm/yyyy">
                                    <span class="input-group-addon"><i class="icons-calendar"></i></span>
                                    <input class="form-control" id="birthdate" type="text"  placeholder="DD/MM/AAAA"/>
                                 </div>
                              </div>
                           </div>                           
                           <div class="form-group col-sm-4">
                              <label for="inversion2" class="title-label">�Durante cu�nto tiempo?</label>
                              <div class="input-group addons-porvenir select-filter">
                                 <span class="input-group-addon"><i class="icons-chrone"></i></span>
                                 <select class="selectpicker btn-white claro min-sel"  data-width="100%" title="selecciona un a�o" id="time">
                                    <option value="2" years="1">1 a�os</option>
                                    <option value="2" years="2">2 a�os</option>
                                    <option value="3" years="3">3 a�os</option>
                                    <option value="3" years="4">4 a�os</option>
                                    <option value="4" years="5">5 a�os</option>
                                    <option value="4" years="6">6 a�os</option>
                                    <option value="4" years="7">7 a�os</option>
                                    <option value="4" years="8">8 a�os</option>
                                    <option value="4" years="9">9 a�os</option>
                                    <option value="5" years="10">10 a�os</option>
                                    <option value="5" years="11">11 a�os</option>
					                <option value="5" years="12">12 a�os</option>
					                <option value="5" years="13">13 a�os</option>
					                <option value="5" years="14">14 a�os</option>
					                <option value="5" years="15">15 a�os</option>  
					                <option value="5" years="16">16 a�os</option>
                                    <option value="5" years="17">17 a�os</option>
                                    <option value="5" years="18">18 a�os</option>
                                    <option value="5" years="19">19 a�os</option>
                                    <option value="5" years="20">20 a�os</option>
                                    <option value="5" years="21">21 a�os</option>
                                    <option value="5" years="22">22 a�os</option>
                                    <option value="5" years="23">23 a�os</option>
                                    <option value="5" years="24">24 a�os</option>
                                    <option value="5" years="25">25 a�os</option>              
                                 </select>
                              </div>
                           </div>
                           <div class="form-group col-sm-4">
                              <div class="cont-inpt">
                                 <label for="inversion" class="title-label">�Con cu�nto quieres empezar?</label>
                                 <div class="input-group addons-porvenir">
                                    <span class="input-group-addon"><i class="icon-pesos"></i></span>
                                    <input id="begin" name="begin" type="text" class="form-control claro number-decimal range-decimal required" title = "Ingrese cantidad" min="100000" data-type="currency" maxlength="15">
                                 </div>
                              </div>
                              <div class="errors hide">El monto debe ser m�nimo de 100.000</div>
                           </div> 
                           <div class="form-group col-sm-4">
                              <div class="cont-inpt">
                                 <label for="inversion" class="title-label">�Cu�nto puedes ahorrar mensualmente?</label>
                                 <div class="input-group addons-porvenir">
                                    <span class="input-group-addon"><i class="icon-pesos"></i></span>
                                    <input id="save" name="save" type="text" class="form-control claro number-decimal range-decimal required" title = "Ingrese cantidad"  min="100000" data-type="currency" maxlength="15">
                                 </div>
                              </div>
                              <div class="errors hide">El monto debe ser m�nimo de 100.000</div>
                           </div>                                                     
                        </form>
                     </div>
                     <div class="form-value hide" id="obj-mi-pension">
                        <form action="envia" class="forms-elm">
                           <div class="form-group col-sm-4 ">
                              <label for="inversion2" class="title-label">G�nero</label>
                              <div class="input-group addons-porvenir">
                                 <span class="input-group-addon"><i class="icons-gender"></i></span>
                                 <select class="selectpicker btn-white claro" title="" data-width="100%" id="gender2">
                                    <option>Hombre</option>
                                    <option>Mujer</option>
                                 </select>
                              </div>
                           </div>
                           <div class="form-group col-sm-4 dates">
                              <label for="inversion3" class="title-label">Fecha de nacimiento</label>
                              <div class="input-group addons-porvenir">
                                 <div id="datepicker2" class=" datepicker input-group date" data-date-format="dd/mm/yyyy">
                                    <span class="input-group-addon"><i class="icons-calendar"></i></span>
                                    <input class="form-control" id="birthdate2" type="text" placeholder="DD/MM/AAAA" />
                                 </div>
                              </div>
                           </div>
                           <div class="form-group col-sm-4">
                              <label for="inversion2" class="title-label">�A qu� edad deseas Retirarse?</label>
                              <div class="input-group addons-porvenir select-filter">
                                 <span class="input-group-addon"><i class="icons-chrone"></i></span>
                                 <select class="selectpicker btn-white claro min-sel" title="Selecciona" data-width="100%" title="" id="age">
                                    <option value="30" data-tokens="30">30 a�os</option>
                                    <option value="31" data-tokens="31">31 a�os</option>
                                    <option value="32" data-tokens="32">32 a�os</option>
                                    <option value="33" data-tokens="33">33 a�os</option>
                                    <option value="34" data-tokens="34">34 a�os</option>
                                    <option value="35" data-tokens="35">35 a�os</option>
                                    <option value="36" data-tokens="36">36 a�os</option>
                                    <option value="37" data-tokens="37">37 a�os</option>
                                    <option value="38" data-tokens="38">38 a�os</option>
                                    <option value="39" data-tokens="39">39 a�os</option>
                                    <option value="40" data-tokens="40">40 a�os</option>
                                    <option value="41" data-tokens="41">41 a�os</option>
                                    <option value="42" data-tokens="42">42 a�os</option>
                                    <option value="43" data-tokens="43">43 a�os</option>
                                    <option value="44" data-tokens="44">44 a�os</option>
                                    <option value="45" data-tokens="45">45 a�os</option>
                                    <option value="46" data-tokens="46">46 a�os</option>
                                    <option value="47" data-tokens="47">47 a�os</option>
                                    <option value="48" data-tokens="48">48 a�os</option>
                                    <option value="49" data-tokens="49">49 a�os</option>
                                    <option value="50" data-tokens="50">50 a�os</option>
                                    <option value="51" data-tokens="51">51 a�os</option>
                                    <option value="52" data-tokens="52">52 a�os</option>
                                    <option value="53" data-tokens="53">53 a�os</option>
                                    <option value="54" data-tokens="54">54 a�os</option>
                                    <option value="55" data-tokens="55">55 a�os</option>
                                    <option value="56" data-tokens="56">56 a�os</option>
                                    <option value="57" data-tokens="57">57 a�os</option>
                                    <option value="58" data-tokens="58">58 a�os</option>
                                    <option value="59" data-tokens="59">59 a�os</option>
                                    <option value="60" data-tokens="60">60 a�os</option>
                                    <option value="61" data-tokens="61">61 a�os</option>
                                    <option value="62" data-tokens="62">62 a�os</option>
                                    <option value="63" data-tokens="63">63 a�os</option>
                                    <option value="64" data-tokens="64">64 a�os</option>
                                    <option value="65" data-tokens="65">65 a�os</option>
                                    <option value="66" data-tokens="66">66 a�os</option>
                                    <option value="67" data-tokens="67">67 a�os</option>
                                    <option value="68" data-tokens="68">68 a�os</option>
                                    <option value="69" data-tokens="69">69 a�os</option>
                                    <option value="70" data-tokens="70">70 a�os</option>
                                    <option value="71" data-tokens="71">71 a�os</option>
                                    <option value="72" data-tokens="72">72 a�os</option>
                                    <option value="73" data-tokens="73">73 a�os</option>
                                    <option value="74" data-tokens="74">74 a�os</option>
                                    <option value="75" data-tokens="75">75 a�os</option>
                                    <option value="76" data-tokens="76">76 a�os</option>
                                    <option value="77" data-tokens="77">77 a�os</option>
                                    <option value="78" data-tokens="78">78 a�os</option>
                                    <option value="79" data-tokens="79">79 a�os</option>
                                    <option value="80" data-tokens="80">80 a�os</option>
                                    <option value="81" data-tokens="76">81 a�os</option>
                                    <option value="82" data-tokens="77">82 a�os</option>
                                    <option value="83" data-tokens="78">83 a�os</option>
                                    <option value="84" data-tokens="79">84 a�os</option>
                                    <option value="85" data-tokens="80">85 a�os</option>
                                 </select>
                              </div>
                              <div class="errors error-age hide">La edad de retiro debe ser mayor a su edad actual</div>
                           </div>
                           <div class="form-group col-sm-4">
                              <div class="cont-inpt">
                                 <label for="inversion" class="title-label">�Con cu�nto quieres empezar?</label>
                                 <div class="input-group addons-porvenir">
                                    <span class="input-group-addon"><i class="icon-pesos"></i></span>
                                    <input id="begin2" type="text" class="form-control claro number-decimal range-decimal" min="100000" data-type="currency" maxlength="15">
                                 </div>
                              </div>
                              <div class="errors hide">El monto debe ser m�nimo de 100.000</div>
                           </div>
                           <div class="form-group col-sm-4">
                              <div class="cont-inpt">
                                 <label for="inversion" class="title-label">�Cuanto puedes ahorrar mensualmente?</label>
                                 <div class="input-group addons-porvenir">
                                    <span class="input-group-addon"><i class="icon-pesos"></i></span>
                                    <input id="save2" type="text" class="form-control claro number-decimal range-decimal" min="100000" data-type="currency" maxlength="15">
                                 </div>
                              </div>
                              <div class="errors hide">El monto debe ser m�nimo de 100.000</div>
                           </div>                           
                           <div class="form-group col-sm-4">
	                           <div class="cont-inpt">
	                              <label for="inversion2" class="title-label">�Cu�l ser�a tu renta mensual esperada?</label>
	                              	<div class="input-group addons-porvenir">
	                                    <span class="input-group-addon"><i class="icon-pesos"></i></span>
	                                    <input id="rent" type="text" class="form-control claro number-decimal range-decimal" min="100000" data-type="currency" maxlength="15">
	                                </div>  	                                
                               </div>
                               <div class="errors hide">El monto debe ser m�nimo de 100.000</div>                           
                           </div>
                        </form>
                     </div>
                     <div class="btn-step">
                        <div  class="prevs show-stripe"><span class="icon-flecha-izq"></span> Volver</div>
                        <div id="slide1" class="next btn-inactive"><span class="icon-flecha-der"></span> Continuar</div>                
                     </div>
                  </div>
                  <div class="itemCarusel">
                     <div class="answer-banner">
                        <p class="text-md"><span class="semi">2.</span> �Con cu�les de las siguientes inversiones has tenido alguna experiencia?</p>
                     </div>
                     <div class="form-answer">
                        <form action="envia" class="forms-check">
                           <ul>
                              <li>
                                 <label class="containers">
                                    <p class="text-mds">Cuenta de ahorros y/o corriente</p>
                                    <input type="checkbox" name="uno" value="1">
                                    <span class="checkmark"></span>
                                 </label>
                              </li>
                              <li>
                                 <label class="containers">
                                    <p class="text-mds">Certificados de dep�sitos a t�rmino (CDT)</p>
                                    <input type="checkbox" name="uno" value="2">
                                    <span class="checkmark"></span>
                                 </label>
                              </li>
                              <li>
                                 <label class="containers">
                                    <p class="text-mds">Bonos y/o Acciones</p>
                                    <input type="checkbox" name="uno" value="3">
                                    <span class="checkmark"></span>
                                 </label>
                              </li>
                              <li>
                                 <label class="containers">
                                    <p class="text-mds">Divisas y/o Commodities</p>
                                    <input type="checkbox" name="uno" value="4">
                                    <span class="checkmark"></span>
                                 </label>
                              </li>
                              <li>
                                 <label class="containers">
                                    <p class="text-mds">Derivados y/o Notas Estructuradas</p>
                                    <input type="checkbox" name="uno" value="5">
                                    <span class="checkmark"></span>
                                 </label>
                              </li>
                           </ul>
                        </form>
                        <div class="luis-responde">
                           <div class="col-xs-12 col-sm-6 fig">
                              <figure>
                                 
                                 <img  src="<%= request.getContextPath()%>/img/rostro_luis.png" />
                                 
                              </figure>
                           </div>
                           <div class="col-xs-12 col-sm-6 mobiTxt">
                              <p class="text-stand"><b>Ten presente que:</b><br>las inversiones requieren un determinado nivel de asesor�a por parte de profesionales expertos y certificados, como nuestros Consultores de inversi�n Porvenir
                              </p>
                           </div>
                        </div>
                     </div>
                     <div class="btn-step">
                        <div  class="prev"><span class="icon-flecha-izq"></span> Volver</div>
                        <div id="slide2" class="next btn-inactive"><span class="icon-flecha-der"></span> Continuar</div>
                     </div>
                  </div>
                  <div class="itemCarusel">
                     <div class="answer-banner">
                        <p class="text-md"><span class="semi">3.</span> �En una eventual desvalorizaci�n de tu portafolio,<br> qu� nivel de p�rdida m�xima estar�as dispuesto a aceptar?</p>
                     </div>
                     <div class="form-answer">
                        <form action="envia" class="forms-check">
                           <ul>
                              <li>
                                 <label class="containers">
                                    <p class="text-mds">No estoy dispuesto a tolerar p�rdidas en mi inversi�n</p>
                                    <input type="radio" name="dos" value="1">
                                    <span class="checkmark"></span>
                                 </label>
                              </li>
                              <li>
                                 <label class="containers">
                                    <p class="text-mds">Hasta un 1% de mi inversi�n</p>
                                    <input type="radio" name="dos" value="2">
                                    <span class="checkmark"></span>
                                 </label>
                              </li>
                              <li>
                                 <label class="containers">
                                    <p class="text-mds">Hasta un 5% de mi inversi�n</p>
                                    <input type="radio" name="dos" value="3">
                                    <span class="checkmark"></span>
                                 </label>
                              </li>
                              <li>
                                 <label class="containers">
                                    <p class="text-mds">Hasta un 10% de mi inversi�n</p>
                                    <input type="radio" name="dos" value="4">
                                    <span class="checkmark"></span>
                                 </label>
                              </li>
                              <li>
                                 <label class="containers">
                                    <p class="text-mds">Hasta un 30% de mi inversi�n</p>
                                    <input type="radio" name="dos" value="5">
                                    <span class="checkmark"></span>
                                 </label>
                              </li>
                           </ul>
                        </form>
                     </div>
                     <div class="btn-step">
                        <div  class="prev"><span class="icon-flecha-izq"></span> Volver</div>
                        <div id="slide3" class="next btn-inactive"><span class="icon-flecha-der"></span> Continuar</div>
                     </div>
                  </div>
                  <div class="itemCarusel">
                     <div class="answer-banner">
                        <p class="text-md"><span class="semi">4.</span> �Durante cu�ntos meses consecutivos estar�as <br>dispuesto a tolerar rentabilidades negativas <br>en tu inversi�n?</p>
                     </div>
                     <div class="form-answer">
                        <form action="envia" class="forms-check">
                           <ul>
                              <li>
                                 <label class="containers">
                                    <p class="text-mds">No estoy dispuesto a tolerar rentabilidades negativas</p>
                                    <input type="radio" name="tres" value="1">
                                    <span class="checkmark"></span>
                                 </label>
                              </li>
                              <li>
                                 <label class="containers">
                                    <p class="text-mds">Un mes</p>
                                    <input type="radio" name="tres" value="2">
                                    <span class="checkmark"></span>
                                 </label>
                              </li>
                              <li>
                                 <label class="containers">
                                    <p class="text-mds">Dos a tres meses</p>
                                    <input type="radio" name="tres" value="3">
                                    <span class="checkmark"></span>
                                 </label>
                              </li>
                              <li>
                                 <label class="containers">
                                    <p class="text-mds">De tres meses a seis meses</p>
                                    <input type="radio" name="tres" value="4">
                                    <span class="checkmark"></span>
                                 </label>
                              </li>
                              <li>
                                 <label class="containers">
                                    <p class="text-mds">M�s de seis meses</p>
                                    <input type="radio" name="tres" value="5">
                                    <span class="checkmark"></span>
                                 </label>
                              </li>
                           </ul>
                        </form>
                     </div>
                     <div class="btn-step">
                        <div  class="prev"><span class="icon-flecha-izq"></span> Volver</div>
                        <div id="slide4" class="next btn-inactive"><span class="icon-flecha-der"></span> Continuar</div>
                     </div>
                  </div>
                  <div class="itemCarusel">
                     <div class="answer-banner">
                        <p class="text-md"><span class="semi">5.</span> �Qu� tipo de inversi�n prefieres?</p>
                     </div>
                     <div class="form-answer">
                        <form action="envia" class="forms-check">
                           <ul>
                              <li>
                                 <label class="containers">
                                    <p class="text-mds">Rentabilidad esperada de 5% con riesgo de desvalorizaci�n de 1%</p>
                                    <input type="radio" name="cuatro" value="1">
                                    <span class="checkmark"></span>
                                 </label>
                              </li>
                              <li>
                                 <label class="containers">
                                    <p class="text-mds">Rentabilidad esperada de 15% con riesgo de desvalorizaci�n de 10%</p>
                                    <input type="radio" name="cuatro" value="3">
                                    <span class="checkmark"></span>
                                 </label>
                              </li>
                              <li>
                                 <label class="containers">
                                    <p class="text-mds">Rentabilidad esperada de 30% con riesgo de desvalorizaci�n de 20%</p>
                                    <input type="radio" name="cuatro" value="5">
                                    <span class="checkmark"></span>
                                 </label>
                              </li>
                           </ul>
                        </form>
                     </div>
                     <div class="btn-step">
                        <div  class="prev"><span class="icon-flecha-izq"></span> Volver</div>
                        <div  class="nexts btn-inactive" id="next-propuesta"><span class="icon-flecha-der"></span> Continuar</div>
                     </div>
                  </div>
               </div>
               
               <div class="external-buttons"></div>
            </div>
         </div>
      </div>
   </div>
</section>

<!-- Propuesta -->
<section class="main_section propuesta hide">
   <div class="center-complement">
      <div class="col-xs-12 col-sm-7">
         <div class="luis3d hidden-xs">
            <figure>               
              <img  src="<%= request.getContextPath()%>/img/rostro_luis_propuesta.png" />                
            </figure>
         </div>
      </div>
      <div class="col-xs-12 col-sm-5">
         <div class="box-desk">
            <form action="envia" class="bx-width">
               <div class="box-name">
                  <h3 class="titleOr grisMedio text-mg"><b><%=nombre %></b></h3>
               </div>
               <div class="box-info">
                  <p class="text-ms text-mg centerMob">De acuerdo a la informaci�n que nos has dado, tu perfil de riesgo es:</p>
                  <div class="luis3d mobile visible-xs">
                     <figure>
                        <img  src="<%= request.getContextPath()%>/img/rostro_luis_propuesta.png"/>
                     </figure>
                  </div>
                  <h3 id="profileType" class="titleOr text-mg"><b>Moderado</b></h3>
                  <p id="profileDesc" class="text-ms centerMob"><span></span><br><br> Tenemos dos propuestas alineadas a tu perfil.</p>
               </div>
            </form>
         </div>
      </div>
   </div>
   <div class="center-complement">
      <div class="col-xs-12">
         <div class="center-elms">
            <p class="text-md text-center">Selecciona la propuesta que m�s se adapte a tus objetivos<br><br></p>
            <div class="col-xs-12 col-sm-6">
               <div class="box-simple box-radius">
                  <h4 class="text-md naranja semi text-mg">Inversi�n delegada</h4>
                  <label class="containers">
                     <p class="text-mds">Realiza tu inversi�n a trav�s de nuestras Alternativas Diversificadas de acuerdo a tu perfil de riesgo y deja en nuestras manos su gesti�n.</p>
              			<input type="radio" name="propuesta" class="prop" value="1">
                     <span class="checkmark"></span>
                  </label>
               </div>
            </div>
            <div class="col-xs-12 col-sm-6">
               <div class="box-simple box-radius">
                  <h4 class="text-md naranja semi text-mg">Inversi�n autogestionada</h4>
                  <label class="containers">
                     <p class="text-mds">Si eres experto realiza tu inversi�n en un portafolio que hemos dise�ado de acuerdo a tu perfil de riesgo y gestiona t� mismo su recomposici�n</p>
                     	<input type="radio" name="propuesta"class="prop" value="2">
                     <span class="checkmark"></span>
                  </label>
               </div>
            </div>
         </div>
         <div class="btn-step">
            <div  class="prev-simulador" id="prev-simulador"><span class="icon-flecha-izq"></span> Volver</div>
            <div class="next-resultados btn-inactive" id="next-simulador"><span class="icon-flecha-der"></span> Continuar</div>
         </div>
      </div>
   </div>
</section>     

<!-- Grafica -->
<section class="main_section propuestaGrafica hide" id="printPdf">
  <img src="<%= request.getContextPath()%>/img/stripe.png" class="header-pdf hide">
  <div class="center-complement">
    <div class="col-xs-12 col-sm-7">  
       <div class="luis3d hidden-xs">
        <figure>
         <img  src="<%= request.getContextPath()%>/img/rostro_luis_propuesta.png" />         
        </figure>
       </div>
    </div>
    <div class="col-xs-12 col-sm-5">
      <div class="box-desk">
        <form action="envia" class="bx-width">
          <div class="box-name">
            <h3 class="titleOr grisMedio text-mg"><span><%=nombre %></span></h3>
          </div>  
           <div class="box-info">
             <div class="luis3d mobile visible-xs">
              <figure>
                <img  src="<%= request.getContextPath()%>/img/rostro_luis_propuesta.png" /> 
              </figure>
             </div>               
            <h3 class="titleOr estandar text-mg"><span>�Genial!</span></h3>
            <p class="text-ms centerMob" id="textProp">De acuerdo a tu perfil de riesgo te presentamos nuestra Alternativa <span class="alternativeOption naranja"></span>, dise�ada para ti.</p><p class="text-ms centerMob"> <br><br>Ten en cuenta que estas proyecciones est�n sujetas a cambios futuros de acuerdo a las condiciones de mercado.</p>          
           </div>
        </form>
      </div>  
    </div>
  </div>  
  <div class="center-complement">   
    <div class="col-xs-12">
      <div class="layout-full">
        <div class="box-simple result-prop">
          <ul class="valCheck">
            <li>
              <p class="text-sm">Objetivo: <span id="inv-target" class="semi naranja">Vivienda</span></p>
            </li>            
            <li>
              <p class="text-sm">Perfil de riesgo: <span id="profile" class="semi naranja"></span></p>
            </li>
            <li>
              <p class="text-sm">Tiempo de inversi�n: <span id="inv-time" class="semi naranja">20 a�os</span></p>
            </li>
            <li>
              <p class="text-sm">Valor esperado: <span id="expectedValue" class="semi naranja">$134.588.399</span></p>
            </li>
            <li id="monthlyRent">
              <p class="text-sm">Renta mensual: <span id="monthlyRentExp" class="semi naranja">$1.250.000</span></p>
            </li>            
          </ul>
        </div>
    <!--     <p class="text-sm text-center legal">Ten en cuenta que estas proyecciones est�n sujetas a <br> cambios futuros de acuerdo a las condiciones de mercado.</p> -->
        <div class="box-simple graficas">
          <div class="col-xs-12 col-sm-12">
            <div class="col-sm-6">
              <h4 class="text-sm naranja semi text-center">Composici�n de tu Alternativa de Inversi�n</h4>
              <div id="chartContainer" style="height: 200px; width: 100%;"></div>
            </div>            
            <div class="col-sm-6 mt-50">
              <h4 class="text-sm naranja semi text-center">Rango de inversion</h4>
              <div id="box-div" class="box-range">
                  <div class="colorSet">
                      <div class="titl-orange cosl-gd"></div>                      
                      <div class="cosl-orange cosl-gd"></div>
                      <div class="cosl-gray cosl-gd"></div>
                      <div class="cosl-green cosl-gd"></div>
                  </div>
                  <div class="diver">
                      <div class="titl-orange cosl-gd alternativeOption"></div>                      
                      <div id="riskfactor0" class="cosl-gd"></div>
                      <div id="riskfactor1" class="cosl-gd"></div>
                      <div id="riskfactor2" class="cosl-gd"></div>               
                  </div>
                  <div class="min-cosl">
                      <div class="titl-orange cosl-gd">M�n</div>                      
                      <div id="min0" class="cosl-gd"></div>
                      <div id="min1" class="cosl-gd"></div>
                      <div id="min2" class="cosl-gd"></div>               
                  </div>
                  <div class="max-cosl">
                      <div class="titl-orange cosl-gd">M�x</div>                      
                      <div id="max0" class="cosl-gd"></div>
                      <div id="max1" class="cosl-gd"></div>
                      <div id="max2" class="cosl-gd"></div>               
                  </div>                                    
              </div>  
              <div id="box-puras" class="box-range hide">
                  <div class="colorSet">
                      <div class="titl-orange cosl-gd"></div>                      
                      <div id="row0" class="cosl-blue cosl-gd hide"></div>
                      <div id="row1" class="cosl-brown cosl-gd hide"></div>
                      <div id="row2" class="cosl-yellow cosl-gd hide"></div>
                      <div id="row3" class="cosl-purple cosl-gd hide"></div>
                      <div id="row4" class="cosl-sky-blue cosl-gd hide"></div>
                  </div>
                  <div class="diver">
                      <div class="titl-orange cosl-gd">Portafolio</div>                      
                      <div id="port0" class="cosl-gd hide"></div>
                      <div id="port1" class="cosl-gd hide"></div>
                      <div id="port2" class="cosl-gd hide"></div>  
                      <div id="port3" class="cosl-gd hide"></div>
                      <div id="port4" class="cosl-gd hide"></div>             
                  </div>
                  <div class="min-cosl">
                      <div class="titl-orange cosl-gd">Composici&oacute;n</div>                      
                      <div id="comp0" class="cosl-gd hide"></div>
                      <div id="comp1" class="cosl-gd hide"></div>
                      <div id="comp2" class="cosl-gd hide"></div>    
                      <div id="comp3" class="cosl-gd hide"></div>
                      <div id="comp4" class="cosl-gd hide"></div>               
                  </div>                                 
              </div>             
            </div>  
            <div class="col-sm-8 mt-20"></div>
          	<div class="col-sm-2 mt-20 coins">
              <h4 class="text-sm naranja semi text-center">Moneda</h4>
              <div class="box-porcentege">
                  <div class="cop col-xs-6">
                      <div id="copPercent" class="colm-orange"></div>
                      <div class="colm-gray">COP</div>
                  </div>
                  <div class="usd col-xs-6">
                      <div id="usdPercent" class="colm-orange"></div>
                      <div class="colm-gray">USD</div>                      
                  </div>
              </div>
           	</div>         
           	<div class="col-sm-2 mt-20"></div>  
          </div>
          <div class="col-xs-12 col-sm-12">
          	<div class="line"></div>
          </div>
          <div class="col-xs-12 col-sm-12">
	          <div class="col-sm-8 mt-4p">
	            <div class="text-md text-center"><h4 class="text-sm naranja semi">Proyecci�n Desempe�o de tu Inversi�n</h4>
	            </div>
	            <div id="chartContainers" style="height: 320px; width: 100%; max-width: 920px;"></div>
	          </div>
	          <div class="col-sm-4 mt-3p">
	              <div class="row  m-10">
	              	<div class="col-xs-12">
	              		 <div class="m-10 text-md text-center">
				              <h4 class="text-sm naranja semi">Ahora combina estas variables</h4>
				        </div>  
	              	</div>
		            <div class="col-xs-12">
		                <div class="slidecontainer">
		                  <div class="text-md text-center">
		                      <h4 class="text-sm naranja semi">Perfil de riesgo</h4>
		                  </div>
		                  <input type="range" min="6" max="30" value="15" class="slider-gradient" id="myNewRange">
		                </div>                
		            </div>
		            <div class="col-xs-12">
		                <div class="slidecontainer">
		                  <div class="text-md text-center">
		                      <h4 class="text-sm semi"><b class="naranja semi">Inversi�n inicial</b><br><br>$ <input id="myRangeValueRev" max="999999999" type="number" value="0" min="0" class="hide"/> <input id="myRangeValueRevMask" type="text" class="number-decimal num-mask" max="999999999" min="0"/></h4>
		                  </div>
		                  <div class="icon-ranges">
		                    <span class="resta-icon inv-minus">-</span>
		                    <input type="range" min="100000" max="10000000" value="0" class="sliders" id="myRange">
		                    <span class="suma-icon inv-plus">+</span>
		                  </div>
		                </div>
		            </div>
		            <div class="col-xs-12">
		                <div class="slidecontainer">
		                  <div class="text-md text-center">
		                      <h4 class="text-sm semi"><b class="naranja semi">Ahorro mensual</b><br><br>$ <input id="myRangeTwoValueRev"  max="999999999" type="number" value="0"  min="0" class="hide"/> <input id="myRangeTwoValueRevMask" type="text" class="number-decimal num-mask" max="999999999" min="0"/></h4>
		                  </div>
		                  <div class="icon-ranges">
		                    <span class="resta-icon sav-minus">-</span>
		                    <input type="range" min="50000" max="5000000" value="0" class="sliders" id="myRangeTwo">
		                    <span class="suma-icon sav-plus">+</span>
		                  </div>
		                </div>
		            </div>
		        </div>
	           </div>
          </div>
          <div class="col-xs-12 col-sm-12"> 
		    <div class="col-sm-4 expected-rent"><h4 class="text-sm naranja semi">Rentabilidad Proyectada: <span class="text-sm expectProfit"> </span></h4></div>		    
		    <div class="col-sm-4"> <div class="boton-primario btn-smash btn-restablecer hidepdf">Restablecer mi Simulaci�n</div></div> 
		    <div class="col-sm-4"> <a class="boton-primario btn-smash hidepdf" href="http://www.porvenir.com.co/web/personas/pensiones-voluntarias" target="_blank">Empieza tu inversi�n aqu�</a></div>
		   </div>      
        </div>
        
       
        
        <img src="<%= request.getContextPath()%>/img/footer-luis.png" class="footer-pdf hide">
                
        <div class="date footer-pdf hide">Fecha y hora de impresi�n: <span class="current-date"></span></div>
        <input type="hidden" id="finalScore"/>
        <input type="hidden" id="yearsToRetireInv"/>
        <input type="hidden" id="origScore"/>
        <input type="hidden" id="origInvestment"/>
        <input type="hidden" id="origSave"/>
        <input type="hidden" id="selectedProp"/>
        <div class="btn-step hidepdf">
          <div class="prev-propuesta text-center" id="prev-propuesta-grafica"><span class="icon-flecha-izq"></span> Volver</div>
          <div class="text-center" id="download-cert"><span class="icon-pdf"></span> Descarga <br>tu resultado</div>         
          <a class="next-final text-center" id="next-final" href="<%=count==3?"#":urlMenu%>"><span class="icon-flecha-der"></span> Continuar</a>
        </div>
        <div class="row top-sp simulationsec hidepdf">
        	<p>La presente simulaci�n es una herramienta suministrada por Porvenir. Es de car�cter informativo, se basa en fuentes de conocimiento p�blico y consideradas confiables. No est� autorizada su reproducci�n por ning�n medio ni ser� ofrecido al cliente como una asesor�a, consejo, recomendaci�n o sugerencia para que tome alguna decisi�n o acci�n al respecto de sus inversiones. En consecuencia, para la toma de decisiones, el cliente debe consultar a sus propios asesores financieros, contables, jur�dicos y tributarios. La administradora, sus accionistas, directivos y/o empleados no son responsables de las consecuencias originadas por el uso de la informaci�n y de los an�lisis aqu� contenidos. Las opiniones y procedimientos  aqu� descritos son v�lidos a la fecha de su publicaci�n y pueden ser modificados en cualquier momento sin previo aviso. Los valores son de referencia indicativa y no constituyen en modo alguno un consejo, recomendaci�n o sugerencia para las decisiones o acciones que tomen sus receptores y no vinculan la responsabilidad de Porvenir. El ejemplo presentado arroja resultados estimados y en ning�n caso garantiza el cumplimiento de un resultado determinado ni aproximado &nbsp;<span id="show-display" class="naranja semi">conozca m�s aqu�</span></p>
        	<div id="display" class="hide">
        		<br/>
        		<ul>
        			<li><p>A trav�s de la anal�tica, se realiza un ejercicio de optimizaci�n de portafolios partiendo de la rentabilidad de los activos utilizando retornos esperados de largo plazo.
        			</p></li>
        			<li><p>Una vez obtenido el resultado del ejercicio te�rico se presenta la plataforma de alternativas de Porvenir a trav�s de la cual el cliente puede acceder a los riesgos determinados.
        			</p></li>
        			<li><p>Para conocer el resultado neto esperado de las Alternativas, se le debe descontar la respectiva comisi�n de administraci�n.
        			</p></li>
        			<li><p>Para ampliar la informaci�n p�ngase en contacto con un Consultor de Porvenir Inversiones.
        			</p></li>        			
        		</ul>
        	</div>
        </div> 
        <div class="row top-sp advance-row hidepdf">#AvancemosJuntos</div>                 
      </div>
    </div>
  </div>
</section>

<c:if test="<%= count == 3 %>">
	<div class="main_section content success-step hide" id="finish-xp">
	    <div class="p-row py-5 justify-content-center">
	        <div class="p-col-12 p-col-md-6 order-1 order-md-0">
	            <img src="<%= request.getContextPath()%>/img/luis-ok.png" alt="luis porvenir" class="img-luis">               
	        </div>
	        <div id="lat-content" class="p-col-12 p-col-md-6 success-data d-flex flex-column flex-nowrap justify-content-end order-0 order-md-1">
	            <h3 class="text-size-31 secondary-text font-medium mb-4"><%= nombre.isEmpty() ? "Hola":nombre %>, &excl;felicitaciones!</h3>
	            <p class="text-size-16 primary-text mb-4">
	               <liferay-ui:message key='xp' /> 
	            </p>
	            <a class="success-button mb-3" href="https://www.porvenir.com.co/web/personas/inicio">Habla con un Consultor</a>
	           <a class="success-button mb-3" href="https://clic.porvenir.com.co/entusmanosahorro">Comienza a ahorrar aqu&iacute;</a> 
	            <!-- <a class="success-button mb-3" href="#">Descarga tu bono</a> -->
	        </div>
	    </div> 
	</div>
</c:if>

<script>
	$("#next-final").click(function(e){   
		var href = $("#next-final").attr('href');
		$.ajax({			  	
			url: '<%=getDiversifiedURL%>',
			data: {
					<portlet:namespace />cmd: '<%= InvestmentConstants.SESSION_SAVE_INVESTMENT%>',
					<portlet:namespace />userName: '<%= nombre %>'					
				  }, 
			type: 'POST'
		});
		$("#finish-xp").removeClass("hide");
	    $(".propuestaGrafica").addClass("hide");
	    if(href === "#")e.preventDefault();
		
     });        

	function getDiversifiedChartValues(_score){
		var _target = getValue("inversion21");		
		var howmuch_begin = $("#myRangeValueRev").val();
		var howmuch_save = $("#myRangeTwoValueRev").val(); 	

		var expected_income = getValue("rent");
		var years = $("#yearsToRetireInv").val();
		
		//$("#myRangeValue").html(formatNumber(howmuch_begin));
		$("#myRangeValueRev").val(howmuch_begin);
		$("#myRangeValueRevMask").val(formatNumber(howmuch_begin));//--
		//$("#myRangeTwoValue").html(formatNumber(howmuch_save));
		$("#myRangeTwoValueRev").val(howmuch_save);
		$("#myRangeTwoValueRevMask").val(formatNumber(howmuch_save));
				
		$.ajax({			  	
			url: '<%=getDiversifiedURL%>',
			data: {	<portlet:namespace />target: _target, 
					<portlet:namespace />cmd: '<%= InvestmentConstants.DIVERSIFIED%>',
					<portlet:namespace />score: _score,
					<portlet:namespace />begin: howmuch_begin,
					<portlet:namespace />save: howmuch_save,
					<portlet:namespace />income: expected_income,
					<portlet:namespace />years: years }, 
			type: 'GET',
			success: function(respuesta) {				
				var obj = JSON.parse(respuesta);
				console.log("obj", obj);
				defineExpectedValue(obj, years);
				defineProjectedIncome(obj.rent);
				var chartData = buildChartJSONArray2(obj.chart);
				
				obj.inversion.sort(function(a, b) {
        	 		var nameA=a.factor_riesgo.toLowerCase(), nameB=b.factor_riesgo.toLowerCase();
        	 	    if (nameA < nameB) 
        	 	        return -1 
        	 	    if (nameA > nameB)
        	 	        return 1
        	 	    return 0 
        		});
				
				for (var i in obj.inversion)
				{
				     var factor_riesgo = obj.inversion[i].factor_riesgo;
				     var min = obj.inversion[i].min;
				     var max = obj.inversion[i].max;
				     
				    $("#riskfactor"+i).html(factor_riesgo);
				    $("#min"+i).html(min.concat(" %"));
				    $("#max"+i).html(max.concat(" %"));
				}
				
				if(obj.alternative[1] != null || !jQuery.isEmptyObject(obj.alternative[1])){
					
					$(".alternativeOption").html(obj.alternative[1].nombre.toLowerCase().capitalize());		
					var descob = obj.alternative[1].descobertura * 100;
					definePercents(descob);
					chart(obj.alternative, '1', chartData);
				}
				
			},
			error: function() {
		        console.log("Error in ajax call");
		    }
		});
	}
	
	function getPureChartValues(_score){
		var _target = getValue("inversion21");		
		var howmuch_begin = $("#myRangeValueRev").val();
		var howmuch_save = $("#myRangeTwoValueRev").val(); 	
		
		var expected_income = getValue("rent");
		var years = $("#yearsToRetireInv").val();
		
		//$("#myRangeValue").html(formatNumber(howmuch_begin));
		$("#myRangeValueRev").val(howmuch_begin); //--
		$("#myRangeValueRevMask").val(formatNumber(howmuch_begin));//--
		//$("#myRangeTwoValue").html(formatNumber(howmuch_save));
		$("#myRangeTwoValueRev").val(howmuch_save);
		$("#myRangeTwoValueRevMask").val(formatNumber(howmuch_save));
				
		$.ajax({			  	
			url: '<%=getDiversifiedURL%>',
			data: {	<portlet:namespace />score: _score, 
					<portlet:namespace />cmd: '<%= InvestmentConstants.PURE%>',
					<portlet:namespace />target: _target, 
					<portlet:namespace />begin: howmuch_begin,
					<portlet:namespace />save: howmuch_save,
					<portlet:namespace />income: expected_income,
					<portlet:namespace />years: years		
					}, 
			type: 'GET',
			success: function(respuesta) {
				
				var origObj = JSON.parse(respuesta);
				console.log("origObj", origObj);
				var obj = origObj.alternative
				defineExpectedValue(origObj, years);
				defineProjectedIncome(origObj.rent);
				var chartData = buildChartJSONArray2(origObj.chart);
				obj.sort(function(a, b) {
	     		    return b.peso - a.peso;
	     		});
				
				for (var i in obj)
				{
				     var nombre = obj[i].alternativa;
				     var peso = obj[i].peso;
				     
				     if(roundWithOneDecimals(peso * 100) > 0){
				    	 $("#row"+i).removeClass("hide");
				   		 $("#port"+i).removeClass("hide").html(nombre);
				   		 $("#comp"+i).removeClass("hide").html(roundWithOneDecimals(peso * 100) + " %");
				     }else{
				    	 $("#row"+i).addClass("hide");
				   		 $("#port"+i).addClass("hide").html("");
				   		 $("#comp"+i).addClass("hide").html("");
				     }
				    percValue = 0;
				   
				    if(nombre.includes("Renta Fija Internacional")){
			 			percValue =  roundWithOneDecimals(peso * 100);
			 			definePercents(percValue);
				    }
				   
				}
				
				chart(obj, '2', chartData);				
				
			},
			error: function() {
		        console.log("Error in ajax call");
		    }
		});
	}
	
	function showGraphic(score){
		  prop = $('input[name="propuesta"]:checked').val();    	 
	      $("#selectedProp").val(prop);
	      defineTextProp(prop);
	        $(".propuestaGrafica").removeClass("hide").show();
	        $(".propuesta").hide();
	        if(prop === '2'){
	        	$('#box-puras').removeClass("hide").show();
	        	$('#box-div').hide();
	        	getPureChartValues(score);
	        }else{
	        	$('#box-div').removeClass("hide").show();
	        	$('#box-puras').hide();
	        	getDiversifiedChartValues(score);  
	        }        
	  }

  $(document).ready(function () {	 	
	  
	  $("#myNewRange").mouseup(function() {
		  customScore = $("#myNewRange").val();
		  console.log("NR: ", customScore);
		  defineProfile(customScore);
		  showGraphic(customScore);
	  });
	  $("#myRange").mouseup(function() {
		  passValue("myRange","myRangeValueRev");
		  passValue("myRange","myRangeValueRevMask");
		  customScore = $("#myNewRange").val();
		  showGraphic(customScore);
	  });
	  $("#myRangeTwo").mouseup(function() {
		  passValue("myRangeTwo","myRangeTwoValueRev");
		  passValue("myRangeTwo","myRangeTwoValueRevMask");
		  customScore = $("#myNewRange").val();
		  showGraphic(customScore);
	  });
	  
	  $('.inv-plus').on('click',function() {
       	  document.getElementById("myRange").stepUp(100000);
       	  var amount = document.getElementById("myRangeValueRev").stepUp(100000);
       	  passValue("myRangeValueRev","myRangeValueRevMask");
       	  customScore = $("#myNewRange").val();
		  showGraphic(customScore);
      });  
	  $(".inv-minus").on('click', function() {
       	  document.getElementById("myRange").stepDown(100000);
       	  document.getElementById("myRangeValueRev").stepDown(100000);
       	  passValue("myRangeValueRev","myRangeValueRevMask");
       	  customScore = $("#myNewRange").val();
		  showGraphic(customScore);
      });  
	  $(".sav-plus").on('click', function() {
       	  document.getElementById("myRangeTwo").stepUp(100000);
       	  document.getElementById("myRangeTwoValueRev").stepUp(100000);
       	  passValue("myRangeTwoValueRev","myRangeTwoValueRevMask");
       	  customScore = $("#myNewRange").val();
		  showGraphic(customScore);
      });  
	  $(".sav-minus").on('click', function() {
       	  document.getElementById("myRangeTwo").stepDown(100000);
       	  document.getElementById("myRangeTwoValueRev").stepDown(100000);
       	  passValue("myRangeTwoValueRev","myRangeTwoValueRevMask");
       	  customScore = $("#myNewRange").val();
		  showGraphic(customScore);
      });  
	  
	  $("#myRangeValueRevMask").focusout(function() {
		   text = $("#myRangeValueRevMask").val().replace(/[^0-9]/g, '');
		   $("#myRangeValueRev").val(text);
		   customScore = $("#myNewRange").val();
		   showGraphic(customScore);
	  });
	  $("#myRangeTwoValueRevMask").focusout(function() {
		   text = $("#myRangeTwoValueRevMask").val().replace(/[^0-9]/g, '');
		   $("#myRangeTwoValueRev").val(text);
		   customScore = $("#myNewRange").val();
		   showGraphic(customScore);
	  });
	 
	  function passValue(originId, destineId){
		  console.log($("#"+originId).val());
		  $("#"+destineId).val($("#"+originId).val());
	  }
	    
	  String.prototype.capitalize = function() {
		    return this.replace(/(?:^|\s)\S/g, function(a) { return a.toUpperCase(); });
	  };
	  
       /*inicializar select*/
       $('.selectpicker').selectpicker();
       
       function valideIsCorrectAge(){
    	   var selected = $('#age option:selected').val(); 
           var birthdate = $("#birthdate2").val();
           
           age = calculateAge(birthdate);
           if(selected > age) return true;
           else return false;
       }
       
       $("#time").change(function(){                 
 		   flag = validateIsEmpty("inversion21","time", "begin", "save");
 		   if(!flag)$("#slide1").removeClass("btn-inactive");//activa bot�n next
 		   else $("#slide1").addClass("btn-inactive");
       });
       $("#age").change(function(){              
           flag = validateIsEmpty("inversion21","age","begin2", "save2", "rent","birthdate2");
		   isAge = valideIsCorrectAge();
           
 		   if(isAge){
 			  $(".error-age ").addClass("hide");
 			  if(!flag)$("#slide1").removeClass("btn-inactive");//activa bot�n next
 		      else $("#slide1").addClass("btn-inactive");
 		   }else {
 			  $(".error-age ").removeClass("hide");
 			 $("#slide1").addClass("btn-inactive");
 		   }
       });       
       
       /*change de [selecciona tu objetivo] */
       
       $("#inversion21").change(function(){ 
           
           var selected = $('#inversion21 option:selected').val();          
           if(selected == "opt3"){
        	   flag = validateIsEmpty("inversion21","age","begin2", "save2", "rent");
        	   if(!flag)$("#slide1").removeClass("btn-inactive");
     		   else $("#slide1").addClass("btn-inactive");
        	   
               $("#obj-estandar").addClass("hide");
               $("#obj-mi-pension").removeClass("hide");
           }else{
        	   flag = validateIsEmpty("inversion21","time", "begin", "save");
     		   if(!flag)$("#slide1").removeClass("btn-inactive");//activa bot�n next
     		   else $("#slide1").addClass("btn-inactive");
     		   
               $("#obj-mi-pension").addClass("hide");
               $("#obj-estandar").removeClass("hide");    
           }       
       });

       $(".bootstrap-select").click(function(){
           $('.slick-active').trigger('click');
       });
       
       $("#begin2,#save2,#rent,#birthdate2").on("keypress keyup blur",function (event) {
    	   flag = validateIsEmpty("inversion21","age","begin2", "save2", "rent", "birthdate2");
 		   isAge = valideIsCorrectAge();
           
 		   if(isAge){
 			  $(".error-age ").addClass("hide");
 			  if(!flag)$("#slide1").removeClass("btn-inactive");//activa bot�n next
 		      else $("#slide1").addClass("btn-inactive");
 		   }else if(!validateIsEmpty("age")) {
 			   console.log("ingres�",$("#birthdate2").val());
 			  $(".error-age ").removeClass("hide");
 		   }
 		   
    	   
    	});
       
	   	$("#begin,#save,#birthdate").on("keypress keyup blur",function (event) {
	   		flag = validateIsEmpty("inversion21","time", "begin", "save", "birthdate");
	      	if(!flag)$("#slide1").removeClass("btn-inactive");//activa bot�n next
	      	else $("#slide1").addClass("btn-inactive");
	    });
       
       
       
        /*
        * Only Numbers function
        */
       function onlyNumbers(text) {  
         // Replace regex '/[^0-9]/g'
         text = text.replace(/[^0-9]/g, '');
   
         // Set to HTML
         var inputResult   = $('.number-decimal');
         inputResult.attr("data") = text;
         $('.slick-active').trigger('click');    
       }
   
     
        var input = $('.number-decimal');
       // Applying brazilian mask
         //  VMasker(input).maskPattern("999.999.999.999");
       
        VMasker(input).maskMoney({
    	   // Decimal precision -> "90"
    	   precision: 0,
    	   // Decimal separator -> ",90"
    	   separator: ',',
    	   // Number delimiter -> "12.345.678"
    	   delimiter: '.',
    	   // Money unit -> "R$ 12.345.678,90"
    	   //unit: '$',
    	   // Money unit -> "12.345.678,90 R$"
    	   //suffixUnit: '$',
    	   // Force type only number instead decimal,
    	   // masking decimals with ",00"
    	   // Zero cents -> "R$ 1.234.567.890,00"
    	   zeroCents: false
    	 });  
            
        $("#show-display").click(function(){
            $("#display").toggleClass("hide");
         });  
       });
     
       $(".range-decimal").keyup(function(){
         var dtn = $(this).val().replace(/[^0-9]/g, '');
         var min = $(this).attr("min");
         var par = parseFloat(min);
         if(dtn >= par){
           $(this).parents(".cont-inpt").next().addClass("hide");
         }else{
           $(this).parents(".cont-inpt").next().removeClass("hide");
         }  
       });       
   
       $(function () {
    	   
    	 var startDate = new Date('01/01/1900');
    	 var fromEndDate = new Date();
    	   
         $(".datepicker").datepicker({ 
               autoclose: true, 
               todayHighlight: true,
               startDate: startDate,
               endDate: fromEndDate
         }).datepicker('update', new Date());
       });

      $("#next-propuesta").click(function(){      
         $(".propuesta").removeClass("hide").show();
         $(".simulador-plan").hide();
      });        
      
      $(".btn-center").click(function(){
         $(".simulador-plan").removeAttr('id');
         $(".simulador-plan").css("display", "block");
          $('.slider').slick({
              speed: 300,
              prevArrow: $('.prev'),
              nextArrow: $('.next'),
              draggable: false,
              accessibility: false,
              dots: true,
              appendDots:'.external-buttons',
              adaptiveHeight: true
        });
        $(".slick-dots li button").text("");          
      });      
      
      $(".prevs").click(function(){
          $(".simulador-plan").attr('id', 'hides');
       }); 
     
      /*******************************************/
       $("#prev-simulador").click(function(){
        $(".simulador-plan").css("display","block").fadeIn();
        $(".propuesta").hide();
     });

    /*radial chart*/
    var chart = function(obj, prop, chartData) {

      CanvasJS.addColorSet("greenShades", ["#b0c3c6", "#ee4f21", "#757575" ]);
      CanvasJS.addColorSet("greenShades2", ["#616466", "#A7B437", "#ee4f21", "#557630", "#C81B1B" ]);
      if (obj != null) {
         if(prop === '1'){
        	 	obj.sort(function(a, b) {
        	 		var nameA=a.factor_riesgo.toLowerCase(), nameB=b.factor_riesgo.toLowerCase();
        	 	    if (nameA < nameB) 
        	 	        return -1 
        	 	    if (nameA > nameB)
        	 	        return 1
        	 	    return 0 
        		});
        	 	
       	 	var dataP = [];
           	for (var i in obj){           		
           		dataP.push({y: roundWithOneDecimals(obj[i].suma_peso * 100), indexLabel : obj[i].factor_riesgo +" - " + roundWithOneDecimals(obj[i].suma_peso * 100) +"%"});
           	}	
        	 
            var chart = new CanvasJS.Chart("chartContainer", {
               exportFileName : "Doughnut Chart",
               animationEnabled : true,
               colorSet : "greenShades",
               title : {},
               legend : {
                  cursor : "pointer",
               },
               axisY : {
                  labelFontSize : 2,
               },
               data : [ {
                  type : "doughnut",
                  innerRadius : 45,
                  indexLabelFontSize : 12,
                  toolTipContent: "{y} %",
                  dataPoints : dataP
               } ]
            });

            chart.render();
         }else if(prop === '2'){
        	obj.sort(function(a, b) {
     		    return b.peso - a.peso;
     		});
        	
        	var dataP = [];
        	for (var i in obj){
        		if(roundWithOneDecimals(obj[i].peso * 100) > 0){
        			dataP.push({y: roundWithOneDecimals(obj[i].peso * 100), indexLabel : obj[i].alternativa +" - " + roundWithOneDecimals(obj[i].peso * 100) +"%"});
        		}
        	}
        	 
            var chart2 = new CanvasJS.Chart("chartContainer", {
               exportFileName : "Doughnut Chart",
               animationEnabled : true,
               colorSet : "greenShades2",
               title : {},
               legend : {
                  cursor : "pointer",
               },
               axisY : {
                  labelFontSize : 2,
               },
               data : [ {
                  type : "doughnut",
                  innerRadius : 45,
                  indexLabelFontSize : 12,
                  toolTipContent: "{y} %",
                  dataPoints : dataP
               } ]
            });

            chart2.render();
         }
         $('.canvasjs-chart-credit').remove();
      }
      
      if(chartData != null){
      /*segunda gr�fica*/    
          
	      CanvasJS.addColorSet("porve", [ "#a7b43791", "#b54e31", "#ee4f2182" ]);
	      
	      var chart1 = new CanvasJS.Chart("chartContainers", {
	          theme: "light2",
	          colorSet: "porve",
	          axisX: {
	        	title: "A�os",
	        	minimum: 0
	           // margin: 2,
	           // interval: 2
	          },
	          axisY: {
	        	title: "Millones",
	            valueFormatString: "$ #,#00,,.",
	            minimum: 0,
	            lineDashType: "solid", 
	            lineThickness: 1 
	          },
	          toolTip: {
	            shared: true,
	            fontSize: 12,
	            contentFormatter: function (e){
	            	 var nDate = new Date();
					 nDate = new Date(nDate.getFullYear(), nDate.getMonth() + 1, 0);
					 nDate.setMonth(nDate.getMonth() + (parseInt(e.entries[0].dataPoint.x))*12);
					 
	            	var content = formatDate(nDate) + "<br/>"
	            	
	            	
					for (var i = 0; i < e.entries.length; i++) {						
						var num = e.entries[i].dataPoint.y[0];
						var doub = parseFloat(num);
						doub = doub.toFixed(0);
						doub = doub + '';
						
						content += e.entries[i].dataSeries.name + ": " + "<strong> $ " + formatNumber(doub)+ "</strong>";
						content += "<br/>";
					}
					return content;
	            }
	          },
	          legend: {
	            dockInsidePlotArea: true,
	            cursor: "pointer",
	            itemclick: toggleDataSeries
	          },
	          /* data: [{
	            type: "rangeArea",
	            markerSize: 0,
	            indexLabelFontSize: 12,
	            showInLegend: true,
	            toolTipContent: "{x}<br><span style=\"color:#6D77AC\">{name}</span><br>Escenario Positivo: $ {y[1]}<br>Escenario Negativo: $ {y[0]}",
	            dataPoints: chartData.area
	
	          }] */
	          data: [{
		            type: "rangeArea",
		            name: "Escenario Positivo",
		            markerSize: 0,
		            indexLabelFontSize: 12,
		            showInLegend: true,
		            //toolTipContent: "Positivo: $ {y[1]}",
		            dataPoints: chartData.positive
		
		       		},
	       			{
		            type: "rangeArea",
		            name: "Escenario Probable",
		            markerSize: 0,
		            indexLabelFontSize: 12,
		            showInLegend: true,
		            //toolTipContent: "Probable: $ {y[0]}",
		            dataPoints: chartData.probable
		
		       		},
	       			{
		            type: "rangeArea",
		            name: "Escenario Negativo",
		            markerSize: 0,
		            indexLabelFontSize: 12,
		            showInLegend: true,
		           // toolTipContent: "Negativo: $ {y[0]}",
		            dataPoints: chartData.negative
		
		       		}]
	      });
	
	      chart1.render();
	      $('.canvasjs-chart-credit').remove();
	      /*  addAverages();
	      
	       function addAverages() {
	         chart1.options.data.push({
	           type: "line",
	           name: "Escenario Probable",
	           showInLegend: true,
	           markerType: "triangle",
	           markerSize: 0,
	           yValueFormatString: "##.0",
	           dataPoints: chartData.line
	         });
	         chart1.render();
	       }   */
      }
	      
        function toggleDataSeries(e) {
          if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
            e.dataSeries.visible = false;
          } else {
            e.dataSeries.visible = true;
          }
          e.chart1.render();
        }
    }
    window.chart();
    $('.canvasjs-chart-credit').remove();

     $("#next-simulador").click(function(){
    	 finalScore =   $("#finalScore").val();
    	 showGraphic(finalScore);
         chart();   
         $('.canvasjs-chart-credit').remove();
     });  
     /*validar los checkmark*/
     $(".checkmark, .text-mds, .containers").click(function(){
          $(this).parents(".box-simple").find(".prop").prop("checked",true);
        var checkOp = $(this).parents(".box-simple").find(".prop").prop("checked");
        if (checkOp == true) {
            $(".box-simple").removeClass("active");
           $(this).parents(".box-simple").addClass("active");
        }
     });   
     
     $("#prev-propuesta-grafica").click(function(){
        $(".propuesta").show();
        $(".propuestaGrafica").hide();
     });  
    
     function generatePDF(elemento, nombre){          
         kendo.drawing.drawDOM($(elemento)).then(function(group){
           setTimeout(function(){ 
             kendo.drawing.pdf.saveAs(group, nombre + '.pdf');
           }, 100);           
         });
     }

     $("#download-cert").click(function(){
      // var valPdf = $(".titleOr:first").text();
       var valPdf = "Inversi�n proyectada";
       $("#printPdf").addClass("hiden-other");
       $(".header-pdf, .footer-pdf").removeClass("hide");
          setTimeout(function(){ 
               $("#printPdf").removeClass("hiden-other");
               $(".header-pdf, .footer-pdf").addClass("hide");
          }, 400);            
       generatePDF("#printPdf", ""+ valPdf+"");   
     });
      
      $(document).keydown(function(objEvent) {
    	    if (objEvent.keyCode == 9) {  //tab pressed
    	        objEvent.preventDefault(); // stops its action
    	    }
      })
      
      
      /*
  * 
  *  
  * Author: Digberth
  * Js agregado para funcionalidades del simulador
  * 
  */

	/** ************************ C�lculo de puntaje ************************** */
	$(".nexts").click(function(){
       
		var _target = getValue("inversion21");
		var myObject = {
			target: _target,
			gender: getValue("gender", _target), 
			birthdate: getValue("birthdate", _target), 
			what_age: getValue("age"),
			howmuch_begin: getValue("begin", _target), 	
			howmuch_save: getValue("save", _target), 			
			howmuch_time: getValue("time", _target),
			expected_income: getValue("rent"),
			investments: getCheckedValues("uno"),
			loss_level: getRadiosValues("dos"),
			tolerate: getRadiosValues("tres"), 
			prefered_investment: getRadiosValues("cuatro")
			
		};		
		 
		
		console.log(myObject);	
		total = calculatePoints(myObject);
		console.log(total);
		defineProfile(total);
		defineTarget(myObject);
		defineTime(myObject);
		
		$("#finalScore").val(total);
		$("#myNewRange").val(total);
		defineInitialInvestment(myObject.howmuch_begin);
		defineMonthlySave(myObject.howmuch_save);
	
        $("#origScore").val(total);
        $("#origInvestment").val(myObject.howmuch_begin);
        $("#origSave").val(myObject.howmuch_save);
        $("#monthlyRentExp").html("$" + formatNumber(myObject.expected_income));
        $(".current-date").html(formatCompleteDate(new Date()));
    });
	
	function defineTime(obj){
		if(obj['target'] == "opt3"){
			age = calculateAge(obj['birthdate']);
			anios_retiro = parseInt(obj['what_age']) - age;
			$('#inv-time').html(Math.round(anios_retiro) + " a�os");
			$('#monthlyRent').show();
		}
		else{
			$('#inv-time').html($("#time option:selected").text());
			$('#monthlyRent').hide();
		}
	}
	
	function defineTarget(obj){
        target = 'Vivienda';
		if(obj['target'] == "opt2") target = "Educaci�n";
		if(obj['target'] == "opt3") target = "Complementar mi pensi&oacute;n";
		if(obj['target'] == "opt4") target = "Otros objetivos de ahorro";
		
		$('#inv-target').html(target);
	}
	
	function defineProfile(total){
		profile = 'Mayor Riesgo';
		desc = 'Eres una persona que quiere invertir y buscas la maximizaci&oacute;n de tu capital, asumiendo niveles de riesgo altos durante un largo tiempo';
		if(parseInt(total) <= 14){
			profile = 'Conservador';
			desc = 'Eres una persona que quiere ahorrar y buscas la conservaci�n y crecimiento  lento de tu capital con un riesgo m�nimo.';
		}
		else if(parseInt(total) >= 15 && parseInt(total) <= 22){
			profile = 'Moderado';
			desc = 'Eres una persona que quiere ahorrar y buscas la conservaci�n y crecimiento de tu capital con un riesgo medio.';
		}
		$('#profile').html(profile);
		$('#profileType b').html(profile);
		$('#profileDesc span').html(desc);
	}
	
	function defineInitialInvestment(newInitialInv){
		$("#myRange").val(newInitialInv);
		//$("#myRangeValue").html(formatNumber(newInitialInv));//--
		$("#myRangeValueRev").val(newInitialInv);
	}
	
	function defineMonthlySave(monthSave){
		$("#myRangeTwo").val(monthSave);
		//$("#myRangeTwoValue").html(formatNumber(monthSave));
		$("#myRangeTwoValueRev").val(monthSave);
	}

	function calculatePoints(obj){
		var age_points = [1,2,2,3,3,4,4,4,4,4,4];
		total = 0;
		age = calculateAge(obj['birthdate']);	
		total += pointsByAge(age);
		
		if(obj['target'] === "opt3"){			
			anios_retiro = parseInt(obj['what_age']) - age;
			$("#yearsToRetireInv").val(anios_retiro);
			if(parseInt(anios_retiro) > 10) total += 5;
			else total += parseInt(age_points[anios_retiro]);
		}else{
			total += parseInt(obj['howmuch_time']); 			
			$("#yearsToRetireInv").val($("#time option:selected").attr("years"));
		}
		
		total += parseInt(Math.max.apply(null, obj['investments'])); // pregunta 2		
		
		total += parseInt(obj['loss_level']); // pregunta 3
		total += parseInt(obj['tolerate']); // Pregunta 4
		total += parseInt(obj['prefered_investment']); // Pregunta 5
		return total;		
	}
	
	function pointsByAge(age){		
		if(parseInt(age) <= 25)return 5;
		else if(parseInt(age) > 25 && parseInt(age) <= 35)return 4;
		else if(parseInt(age) > 35 && parseInt(age) <= 45)return 3;
		else if(parseInt(age) > 45 && parseInt(age) <= 60)return 2;
		return 1;		
	}
	
	function calculateAge(birthdate){
		
		var fechaNace = getFormatDate(birthdate);
		var fechaActual = new Date()
		
		var mes = fechaActual.getMonth();
		var dia = fechaActual.getDate();
		var anio = fechaActual.getFullYear();
		
		fechaActual.setDate(dia);
		fechaActual.setMonth(mes);
		fechaActual.setFullYear(anio);
		
		var diff_ms = fechaActual - fechaNace;
		var age_dt = new Date(diff_ms); 
		  
	    return Math.abs(age_dt.getUTCFullYear() - 1970);
		
	}
	
	function getFormatDate(dateString){
		var dateParts = dateString.split("/");
		var dateObject = new Date(+dateParts[2], dateParts[1] - 1, +dateParts[0]); 
		return dateObject;		
	}
  
  /** ******************* activar y desactivar Next ************************ */

	  $('input[name="uno"]').click(function(){
		  if ($('input[name="uno"]:checked').length) {
			  $("#slide2").removeClass("btn-inactive");
		  }else{
			 $("#slide2").addClass("btn-inactive");
		  }
	  });   
	  $('input[name="dos"]').click(function(){
		  if ($('input[name="dos"]:checked').length) {
			  $("#slide3").removeClass("btn-inactive");
		  }
	  });
	  $('input[name="tres"]').click(function(){
		  if ($('input[name="tres"]:checked').length) {
			  $("#slide4").removeClass("btn-inactive");
		  }
	  });
	  $('input[name="cuatro"]').click(function(){
		  if ($('input[name="cuatro"]:checked').length) {
			  $("#next-propuesta").removeClass("btn-inactive");
		  }
	  });	  
	  $('input[name="propuesta"]').click(function(){
		  if ($('input[name="propuesta"]:checked').length) {
			  $("#next-simulador").removeClass("btn-inactive");
		  }
	  });
     
  /** ****************** Funciones de obtenci�n de datos ************ */
  
	  function getValue($fieldId, $target){
		value = "";
	    if($target == "opt3")
	    	value = $('#' + $fieldId + '2').val();
	    else
	    	value = $('#' + $fieldId).val();
	    if(value != null)  return value.replace(/\./g, '')
	    else return "";
	  }
	  
	  function getCheckedValues($fieldName){
	    var chekeds = [];
	        $('input[name="'+ $fieldName +'"]:checked').each(function(i){
	          chekeds.push($(this).val());
	        });
	        return chekeds;   
	  }
	  
	  function getRadiosValues($fieldName){     
	       return $('input[name="'+ $fieldName +'"]:checked').val();
	  }
	  
	  /********************************************************/
	  function defineProjectedIncome(value){
		  percentAvg = (Math.round(value * 1000)/10); 
		  $(".expectProfit").html(percentAvg + "% E. A.");
		  
	  }
	  function definePercents(value){
		  usdP = roundWithOneDecimals(value); 
		  copP = 100 - usdP;
		  copP = roundWithOneDecimals(copP); 
		  $("#usdPercent").html(usdP + "%");
		  $("#copPercent").html(copP + "%");			
	  }	 
	  
	  function roundWithOneDecimals(value){
		  return (Math.round(value * 10)/10);		  
	  }
	  
	  function buildChartJSONArray(list){
		  var areaPoints = [];
		  var linePoints = [];
		  for (var i in list){
			 // var nDate = new Date();
			 // nDate = new Date(nDate.getFullYear(), nDate.getMonth() + 1, 0);
			 // nDate.setMonth(nDate.getMonth() + (parseInt(i)+1)*12);
			  
			  var areaObj = { x: parseInt(i)+1, y: [list[i].negative, list[i].positive] };
			  var lineObj = { x: parseInt(i)+1, y: list[i].probable };
			  areaPoints.push(areaObj);
			  linePoints.push(lineObj);
		  }		  
		  var chartData = {area: areaPoints, line: linePoints};	
		  return chartData;
	  }
	  
	  function buildChartJSONArray2(list){
		  var positiveAreaPoints = [];
		  var negativeAreaPoints = [];
		  var probableAreaPoints = [];
		  for (var i in list){
			  
			  var positiveArea = { x: parseInt(i), y: [list[i].positive, list[i].probable] };
			  var negativeArea = { x: parseInt(i), y: [list[i].negative, list[i].probable] };
			  var probableArea = { x: parseInt(i), y: [list[i].probable, list[i].probable] };
			  
			  positiveAreaPoints.push(positiveArea);
			  negativeAreaPoints.push(negativeArea);
			  probableAreaPoints.push(probableArea);
		  }		  
		  var chartData = {positive: positiveAreaPoints, negative: negativeAreaPoints, probable: probableAreaPoints};	
		  return chartData;
	  }
	  
	  $(".btn-restablecer").click(function(){	   
		  var score = $("#origScore").val();
		  var howmuch_begin = $("#origInvestment").val();
		  var howmuch_save = $("#origSave").val(); 	
		  
		  $("#myNewRange").val(score);
	      $("#myRangeValueRev").val(howmuch_begin);
		  $("#myRangeTwoValueRev").val(howmuch_save); 
		  
		  //$("#myRangeValue").html(howmuch_begin);
		  $("#myRange").val(howmuch_begin); //--
		  //$("#myRangeTwoValue").html(howmuch_save);
		  $("#myRangeTwo").val(howmuch_save);
		  defineProfile(score);
	      showGraphic(score);
	  });
	  
	 
	  $(".number-decimal").on("keypress keyup blur",function (event) {
			$(this).val($(this).val().replace(/[^0-9\.]/g,''));
			if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
				event.preventDefault();
			}		
			if(event.which == 8 || event.which == 46) {
				$(this).trigger({
			        type: 'keyup',
			        which: 27 // Escape key
			    });
		    }
			
      });
	  
	  function formatNumber(num) {
		  return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
	  }
	  
	  function validateIsEmpty(...fieldNames){		  
		  var numFields = ["begin", "save", "begin2", "save2", "rent"];
		  
		  var isComplete = false;
		  for (var name of fieldNames) {
			  var containsField = (numFields.indexOf(name) > -1);
			  value = $("#"+name).val();
			  if(value == "") return true;
			  if(containsField){
				  if(value.replace(/\./g, '') < 100000) return true;
			  }
			  
		  }
		  return isComplete;		  
	  }
	  
	  function formatDate(date) {
		  var monthNames = [
		    "Ene", "Feb", "Mar",
		    "Abr", "May", "Jun", "Jul",
		    "Ago", "Sep", "Oct",
		    "Nov", "Dic"
		  ];

		  var day = date.getDate();
		  var monthIndex = date.getMonth();
		  var year = date.getFullYear();

		  return day + ' ' + monthNames[monthIndex] + ' ' + year;
		}
	  
	  function defineExpectedValue(obj, time){
		  value = "";
		  if(obj!=null){
			  value = obj.chart[time].probable;
			  var doub = parseFloat(value);
			  doub = doub.toFixed(0);
			  doub = doub + '';
			  $("#expectedValue").html("$"+formatNumber(doub));
		  }
	  }
	  
	  function defineTextProp(prop){
		  if(prop === "2"){
				$('#textProp').html('De acuerdo a tu perfil de riesgo hemos dise�ado el siguiente Portafolio de inversi�n para ti.');
		  }else{
			  $('#textProp').html('De acuerdo a tu perfil de riesgo te presentamos nuestra Alternativa <span class="alternativeOption naranja"></span>, dise�ada para ti.');
		  }
	  }	  
	  
	  function validateJustNumbers(e){
		  //var dtn = $(e).html().replace(/[^0-9]/g, '');
		  var dtn = $(e).html();
		  $(e).html(formatNumber(dtn))
		  $(e).html().focus();
	  }
	  
	  function formatCompleteDate(date) {
		  var hours = date.getHours();
		  var minutes = date.getMinutes();
		  var ampm = hours >= 12 ? 'pm' : 'am';
		  hours = hours % 12;
		  hours = hours ? hours : 12; // the hour '0' should be '12'
		  minutes = minutes < 10 ? '0'+minutes : minutes;
		  var strTime = hours + ':' + minutes + ' ' + ampm;
		  return  date.getDate() + "/"+ parseInt(date.getMonth()+1) + "/" + date.getFullYear() + "  " + strTime;
		}
	
	 <%--  window.addEventListener('beforeunload', function (e) { 
		  
		  $.ajax({			  	
				url: '<%=getDiversifiedURL%>',
				data: {
						<portlet:namespace />cmd: '<%= InvestmentConstants.SESSION_CLEAR%>',
					  }, 
				type: 'POST'
			});
		  //e.preventDefault(); 
          //e.returnValue = ''; 
      });      --%>
      
</script>
<%@ include file="init.jsp" %>

<liferay-portlet:actionURL portletConfiguration="<%=true %>" var="configActionURL" />

<aui:form action="<%= configActionURL %>" method="post" name="fm">
    <aui:input name="<%= Constants.CMD %>" type="hidden" value="<%= Constants.UPDATE %>" />     
    <aui:input label="URL de p�gina de men�" name="<%= InvestmentSimulatorPortletKeys.PREFERENCE_MENU_URL %>" value="<%=urlMenu%>" />
     
   <aui:button-row>
       <aui:button type="submit"></aui:button>
   </aui:button-row>
</aui:form>
package co.com.vasslatam.investmentsimulator.configuration;

import com.liferay.portal.kernel.portlet.ConfigurationAction;
import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;
import com.liferay.portal.kernel.util.ParamUtil;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;

import co.com.vasslatam.investmentsimulator.portlet.constants.InvestmentSimulatorPortletKeys;

@Component(configurationPolicy = ConfigurationPolicy.OPTIONAL, immediate = true, property = { "javax.portlet.name="
		+ InvestmentSimulatorPortletKeys.INVESTMENTSIMULATOR }, service = ConfigurationAction.class)

public class InvestmentSimulatorConfig extends DefaultConfigurationAction {

	@Override
	public void processAction(PortletConfig portletConfig, ActionRequest actionRequest, ActionResponse actionResponse)
			throws Exception {

		String urlMenu = ParamUtil.getString(actionRequest, InvestmentSimulatorPortletKeys.PREFERENCE_MENU_URL);
		setPreference(actionRequest, InvestmentSimulatorPortletKeys.PREFERENCE_MENU_URL, urlMenu);

		super.processAction(portletConfig, actionRequest, actionResponse);
	}
}

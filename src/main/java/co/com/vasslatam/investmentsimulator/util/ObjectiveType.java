package co.com.vasslatam.investmentsimulator.util;

public enum ObjectiveType {
	VIVIENDA("opt1"),
	EDUCACION("opt2"),
	COMPLEMENTAR_MI_PENSION("opt3"),
	OTROS_OBJETIVOS_AHORRO("opt4");
	
	private final String value;       

    private ObjectiveType(String s) {
        value = s;
    }

	public String getValue() {
		return value;
	}

}

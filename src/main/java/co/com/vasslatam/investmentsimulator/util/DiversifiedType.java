package co.com.vasslatam.investmentsimulator.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;


public enum DiversifiedType {

	
	CONSERVATIVE("DIVERSIFICADO CONSERVADOR", 1),	
	BASIC("DIVERSIFICADO B�SICO", 2), 
	DYNAMIC("DIVERSIFICADO DIN�MICO", 3),
	EXTREM("DIVERSIFICADO EXTREMO", 4);

	public static DiversifiedType getByValue(String type) {		
		
		for (DiversifiedType p : values()) {
			if (p.getValue().toLowerCase().equals(type.toLowerCase())) {
				return p;
			}
		}		
		return null;
	}

	private final String value;
	private final int numValue;

	private static final Map<String, DiversifiedType> lookup = new HashMap<String, DiversifiedType>();

	static {
		for (DiversifiedType type : DiversifiedType.values()) {
			lookup.put(type.getValue(), type);
		}
	}

	private static final Map<Integer, DiversifiedType> lookup2 = new HashMap<Integer, DiversifiedType>();

	static {
		for (DiversifiedType type : DiversifiedType.values()) {
			lookup2.put(type.getNumValue(), type);
		}
	}

	private DiversifiedType(String s, int i) {
		value = s;
		numValue = i;
	}

	public static Optional<DiversifiedType> getFromValue(String value) {
		return Optional.ofNullable(lookup.get(value));
	}

	public static Optional<DiversifiedType> getFromIntValue(int value) {
		return Optional.ofNullable(lookup2.get(value));
	}

	public String getValue() {
		return value;
	}

	public int getNumValue() {
		return numValue;
	}

	public static Set<String> asSet() {
		return lookup.keySet();
	}

}

package co.com.vasslatam.investmentsimulator.domain;

public class InvestmentConstants {
	
	public static final String DDLKEY_COLUMN_NAME = "Nombre";
	public static final String DDLKEY_COLUMN_VALUE = "Valor";
	public static final String HEADER_SERVICE_TRANSACTION = "Servicetransaction";
	public static final String XML_TOKEN_CLOSE = "<%";
	public static final String XML_TOKEN_OPEN = "%>";
	public static final String NAME = "name";
	
	// Portafolios diversificados CAMPOS DDL
	public static final String ID = "id";
	public static final String DIVERSIFIED_PORTFOLIO = "portafolio_diversificado";
	public static final String RISK_FACTOR = "factor_riesgo";
	public static final String MIN = "min";
	public static final String MAX = "max";
	
	public static final String INFLATION_KEY = "inflacionKey";
	
	//Nombre de DDL
	public static final String DDL_NAME_DIVERSIFICADOS = "Benchmark Diversificados";
	public static final String DDL_NAME_INFLATION = "Inflacion";
	

	public static final String SCORE = "score";
	public static final String PURE = "Pura";
	public static final String DIVERSIFIED = "Div";
	public static final String SESSION_SAVE_INVESTMENT = "Inv";
	public static final String SESSION_CLEAR = "CLEAR";
	
	
	public static final int INITIAL_YEAR = 0;
	
	public static final String TARGET = "target";
	public static final String INITIAL_INVESTMENT = "begin";
	public static final String MONTHLY_SAVINGS = "save";
	public static final String EXPECTED_INCOME = "income";
	public static final String TIME = "years";
	
	

}

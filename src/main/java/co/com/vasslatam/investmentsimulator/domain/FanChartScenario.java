package co.com.vasslatam.investmentsimulator.domain;

public class FanChartScenario {
	
	double negative;
	double probable;
	double positive;
	
	
	public FanChartScenario(double negative, double probable, double positive) {
		super();
		this.negative = negative;
		this.probable = probable;
		this.positive = positive;
	}
	
	
	public double getNegative() {
		return negative;
	}
	public void setNegative(double negative) {
		this.negative = negative;
	}
	public double getProbable() {
		return probable;
	}
	public void setProbable(double probable) {
		this.probable = probable;
	}
	public double getPositive() {
		return positive;
	}
	public void setPositive(double positive) {
		this.positive = positive;
	}	
}

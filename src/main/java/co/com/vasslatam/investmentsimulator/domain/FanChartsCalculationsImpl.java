package co.com.vasslatam.investmentsimulator.domain;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.porvenir.luis.portafolio.model.ChartDiversificado;
import com.porvenir.luis.portafolio.model.ChartPura;
import com.porvenir.luis.portafolio.service.ChartDiversificadoLocalServiceUtil;
import com.porvenir.luis.portafolio.service.ChartPuraLocalServiceUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.osgi.service.component.annotations.Component;

import co.com.vasslatam.investmentsimulator.util.ObjectiveType;

@Component(immediate = true, property = {}, service = IFanChartsCalculations.class)

public class FanChartsCalculationsImpl implements IFanChartsCalculations {

	private static final Log _log = LogFactoryUtil.getLog(FanChartsCalculationsImpl.class);
	private static final int MAX_ITERATIONS = 70;

	@Override
	public double getPaidAnnuity(int timeInYears, int years_RetireOrInvestment, double monthlySavingsAmount) {
		return timeInYears > years_RetireOrInvestment ? 0 : monthlySavingsAmount;
	}

	@Override
	public double getAnnuityDiscount(String savingReason, double inflation, int timeInYears,
			int years_RetireOrInvestment, double expectedAnnualIncome) {

		if (!savingReason.toLowerCase().equals(ObjectiveType.COMPLEMENTAR_MI_PENSION.getValue().toLowerCase()))
			return 0;
		return timeInYears > years_RetireOrInvestment ? expectedAnnualIncome * Math.pow((1 + inflation), timeInYears)
				: 0;
	}

	@Override
	public double getScenarioValue(double criteria, double previousAmount, int timeInYears,
			int years_RetireOrInvestment, double monthlySavingsAmount, double expectedAnnualIncome, String savingReason,
			double inflation) {
		return (1 + criteria) * previousAmount
				+ getPaidAnnuity(timeInYears, years_RetireOrInvestment, monthlySavingsAmount) - getAnnuityDiscount(
						savingReason, inflation, timeInYears, years_RetireOrInvestment, expectedAnnualIncome);
	}

	@Override
	public List<FanChartScenario> getChartData(String diversifiedPortfolio, int pureScore, double previousAmount,
			int years_RetireOrInvestment, double monthlySavingsAmount, double expectedAnnualIncome, String savingReason,
			double inflation) {

		List<FanChartScenario> data = new ArrayList<FanChartScenario>();
		data.add(new FanChartScenario(previousAmount, previousAmount, previousAmount));

		try {
			double lowerValue = previousAmount;
			double mediumValue = previousAmount;
			double higherValue = previousAmount;
			int limit = years_RetireOrInvestment;

			for (int i = 1; i <= limit; i++) {
				double coefficients[] = getCoefficient(diversifiedPortfolio, pureScore, i);

				lowerValue = getScenarioValue(coefficients[0], lowerValue, i, years_RetireOrInvestment,
						monthlySavingsAmount, expectedAnnualIncome, savingReason, inflation);
				mediumValue = getScenarioValue(coefficients[1], mediumValue, i, years_RetireOrInvestment,
						monthlySavingsAmount, expectedAnnualIncome, savingReason, inflation);
				higherValue = getScenarioValue(coefficients[2], higherValue, i, years_RetireOrInvestment,
						monthlySavingsAmount, expectedAnnualIncome, savingReason, inflation);
				
				FanChartScenario fanChartScenario = new FanChartScenario(lowerValue, mediumValue, higherValue);
				data.add(fanChartScenario);

				if (savingReason.toLowerCase().equals(ObjectiveType.COMPLEMENTAR_MI_PENSION.getValue().toLowerCase())) {
					limit = (!areNegativeValues(lowerValue, mediumValue, higherValue)) ? limit += 1 : i;
					if(i >= MAX_ITERATIONS)break;
				}
			}

		} catch (Exception e) {
			_log.error("Error in FanChartsCalculationsImpl:getChartData ", e);
		}

		return data;
	}

	private double[] getCoefficient(String portfolioName, int score, int time) {
		double coefficients[] = new double[3];
		double lowerCoefficient = 0;
		double probableCoefficient = 0;
		double higherCoefficient = 0;
		try {
			if (score == 0) {
				ChartDiversificado chartDiversificado = ChartDiversificadoLocalServiceUtil
						.findByName_Time(portfolioName, time);
				lowerCoefficient = chartDiversificado.getInferior();
				probableCoefficient = chartDiversificado.getMedio();
				higherCoefficient = chartDiversificado.getSuperior();

			} else {
				ChartPura chartPura = ChartPuraLocalServiceUtil.findByScore_Time(score, time);
				lowerCoefficient = chartPura.getInferior();
				probableCoefficient = chartPura.getMedio();
				higherCoefficient = chartPura.getSuperior();
			}

			coefficients[0] = lowerCoefficient;
			coefficients[1] = probableCoefficient;
			coefficients[2] = higherCoefficient;

		} catch (Exception e) {
			_log.error("Error in FanChartsCalculationsImpl:getCoefficient ", e);
		}

		return coefficients;
	}

	@Override
	public double getProjectedProfitability(String diversifiedPortfolio, int pureScore, int years_RetireOrInvestment) {
		double average = 0;
		if (pureScore == 0) {
			List<ChartDiversificado> chartDivs = ChartDiversificadoLocalServiceUtil.getByName(diversifiedPortfolio, years_RetireOrInvestment);
			average = chartDivs.stream().collect(Collectors.averagingDouble(a -> a.getMedio()));
		}else {
			List<ChartPura> chartPures = ChartPuraLocalServiceUtil.getByScore(pureScore, years_RetireOrInvestment);
			average = chartPures.stream().collect(Collectors.averagingDouble(a -> a.getMedio()));
		}
		return average;
	}
	
	private boolean areNegativeValues(double a, double b, double c) {
		return (a < 0 && b < 0 && c < 0);
	}

}

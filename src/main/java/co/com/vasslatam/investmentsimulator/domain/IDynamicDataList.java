package co.com.vasslatam.investmentsimulator.domain;

import com.liferay.dynamic.data.lists.model.DDLRecord;
import com.liferay.portal.kernel.json.JSONArray;

import java.util.List;

import aQute.bnd.annotation.ProviderType;


@ProviderType
public interface IDynamicDataList {

	List<DDLRecord> getDDLRecords(String ddlName);
	
	JSONArray getDDLRecordValues(String ddlName);
	
	JSONArray filterByKeyValue(String ddlName, String key, String value);
	
	double getFirstValueByKey(String ddlName, String key);
}

package co.com.vasslatam.investmentsimulator.domain;

import java.util.List;

import aQute.bnd.annotation.ProviderType;

@ProviderType
public interface IFanChartsCalculations {
	
	double getScenarioValue(double criteria, double previousAmount, int timeInYears,
			int years_RetireOrInvestment, double monthlySavingsAmount, double expectedAnnualIncome, String savingReason, double inflation);
	
	double getPaidAnnuity(int timeInYears, int years_RetireOrInvestment, double monthlySavingsAmount);

	double getAnnuityDiscount(String savingReason, double inflation, int timeInYears, int years_RetireOrInvestment,
			double monthlySavingsAmount);
	
	List<FanChartScenario> getChartData(String diversifiedPortfolio, int pureScore, double previousAmount,
			int years_RetireOrInvestment, double monthlySavingsAmount, double expectedAnnualIncome, String savingReason, double inflation);
	
	double getProjectedProfitability(String diversifiedPortfolio, int pureScore, int years_RetireOrInvestment);

}

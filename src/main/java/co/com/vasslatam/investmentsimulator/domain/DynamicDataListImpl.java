package co.com.vasslatam.investmentsimulator.domain;

import com.liferay.dynamic.data.lists.model.DDLRecord;
import com.liferay.dynamic.data.lists.model.DDLRecordSet;
import com.liferay.dynamic.data.lists.service.DDLRecordLocalServiceUtil;
import com.liferay.dynamic.data.lists.service.DDLRecordSetLocalServiceUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.StringPool;

import java.util.Collections;
import java.util.List;

import org.osgi.service.component.annotations.Component;

@Component(immediate = true, property = {}, service = IDynamicDataList.class)

public class DynamicDataListImpl implements IDynamicDataList {

	private static final Log _log = LogFactoryUtil.getLog(DynamicDataListImpl.class);

	@Override
	public List<DDLRecord> getDDLRecords(String name) {
		try {
			DynamicQuery dq = DDLRecordSetLocalServiceUtil.dynamicQuery();
			dq.add(RestrictionsFactoryUtil.like(InvestmentConstants.NAME,
					InvestmentConstants.XML_TOKEN_OPEN.concat(name).concat(InvestmentConstants.XML_TOKEN_CLOSE)));
			List<DDLRecordSet> recordSets = DDLRecordSetLocalServiceUtil.dynamicQuery(dq);

			if (recordSets.isEmpty())
				return Collections.emptyList();

			DDLRecordSet dDLRecordSet = recordSets.get(0);
			List<DDLRecord> dDLRecords = DDLRecordLocalServiceUtil.getRecords(dDLRecordSet.getRecordSetId());

			return dDLRecords;

		} catch (Exception e) {
			_log.error("Error while retrieving recordSet ", e);
			return Collections.emptyList();
		}

	}

	@Override
	public JSONArray getDDLRecordValues(String name) {
		List<DDLRecord> records = getDDLRecords(name);
		JSONArray jsonArray = JSONFactoryUtil.createJSONArray();
		for (DDLRecord ddlRecord : records) {
			try {
				JSONObject jsonObject = JSONFactoryUtil.createJSONObject();

				jsonObject.put(InvestmentConstants.ID,
						ddlRecord.getDDMFormFieldValues(InvestmentConstants.ID).parallelStream().findFirst().get()
								.getValue()
								.getString(ddlRecord.getDDMFormFieldValues(InvestmentConstants.ID).parallelStream()
										.findFirst().get().getValue().getDefaultLocale()) != null ? ddlRecord
												.getDDMFormFieldValues(InvestmentConstants.ID).parallelStream()
												.findFirst().get().getValue()
												.getString(ddlRecord.getDDMFormFieldValues(InvestmentConstants.ID)
														.parallelStream().findFirst().get().getValue()
														.getDefaultLocale())
												: StringPool.BLANK);
				jsonObject.put(InvestmentConstants.DIVERSIFIED_PORTFOLIO, ddlRecord
						.getDDMFormFieldValues(InvestmentConstants.DIVERSIFIED_PORTFOLIO).parallelStream().findFirst()
						.get().getValue()
						.getString(ddlRecord.getDDMFormFieldValues(InvestmentConstants.DIVERSIFIED_PORTFOLIO)
								.parallelStream().findFirst().get().getValue().getDefaultLocale()) != null ? ddlRecord
										.getDDMFormFieldValues(InvestmentConstants.DIVERSIFIED_PORTFOLIO)
										.parallelStream().findFirst().get().getValue()
										.getString(ddlRecord
												.getDDMFormFieldValues(InvestmentConstants.DIVERSIFIED_PORTFOLIO)
												.parallelStream().findFirst().get().getValue().getDefaultLocale())
										: StringPool.BLANK);
				jsonObject.put(InvestmentConstants.RISK_FACTOR,
						ddlRecord.getDDMFormFieldValues(InvestmentConstants.RISK_FACTOR).parallelStream().findFirst()
								.get().getValue()
								.getString(ddlRecord.getDDMFormFieldValues(InvestmentConstants.RISK_FACTOR)
										.parallelStream().findFirst().get().getValue().getDefaultLocale()) != null
												? ddlRecord.getDDMFormFieldValues(InvestmentConstants.RISK_FACTOR)
														.parallelStream().findFirst().get().getValue()
														.getString(ddlRecord
																.getDDMFormFieldValues(InvestmentConstants.RISK_FACTOR)
																.parallelStream().findFirst().get().getValue()
																.getDefaultLocale())
												: StringPool.BLANK);
				jsonObject.put(InvestmentConstants.MIN,
						ddlRecord.getDDMFormFieldValues(InvestmentConstants.MIN).parallelStream().findFirst().get()
								.getValue()
								.getString(ddlRecord.getDDMFormFieldValues(InvestmentConstants.MIN).parallelStream()
										.findFirst().get().getValue().getDefaultLocale()) != null ? ddlRecord
												.getDDMFormFieldValues(InvestmentConstants.MIN).parallelStream()
												.findFirst().get().getValue()
												.getString(ddlRecord.getDDMFormFieldValues(InvestmentConstants.MIN)
														.parallelStream().findFirst().get().getValue()
														.getDefaultLocale())
												: StringPool.BLANK);
				jsonObject.put(InvestmentConstants.MAX,
						ddlRecord.getDDMFormFieldValues(InvestmentConstants.MAX).parallelStream().findFirst().get()
								.getValue()
								.getString(ddlRecord.getDDMFormFieldValues(InvestmentConstants.MAX).parallelStream()
										.findFirst().get().getValue().getDefaultLocale()) != null ? ddlRecord
												.getDDMFormFieldValues(InvestmentConstants.MAX).parallelStream()
												.findFirst().get().getValue()
												.getString(ddlRecord.getDDMFormFieldValues(InvestmentConstants.MAX)
														.parallelStream().findFirst().get().getValue()
														.getDefaultLocale())
												: StringPool.BLANK);
				jsonArray.put(jsonObject);

			} catch (Exception e) {
				_log.error("Error while retrieving recordSet values ", e);
			}
		}
		return jsonArray;
	}

	@Override
	public JSONArray filterByKeyValue(String ddlName, String key, String value) {
		List<DDLRecord> records = getDDLRecords(ddlName);
		JSONArray jsonArray = JSONFactoryUtil.createJSONArray();
		for (DDLRecord ddlRecord : records) {
			try {
				JSONObject jsonObject = JSONFactoryUtil.createJSONObject();
				
				if(ddlRecord.getDDMFormFieldValues(key).parallelStream().findFirst().get()
				.getValue()
				.getString(ddlRecord.getDDMFormFieldValues(key).parallelStream()
						.findFirst().get().getValue().getDefaultLocale()).equals(value)) {
			
					jsonObject.put(InvestmentConstants.ID,
							ddlRecord.getDDMFormFieldValues(InvestmentConstants.ID).parallelStream().findFirst().get()
									.getValue()
									.getString(ddlRecord.getDDMFormFieldValues(InvestmentConstants.ID).parallelStream()
											.findFirst().get().getValue().getDefaultLocale()) != null ? ddlRecord
													.getDDMFormFieldValues(InvestmentConstants.ID).parallelStream()
													.findFirst().get().getValue()
													.getString(ddlRecord.getDDMFormFieldValues(InvestmentConstants.ID)
															.parallelStream().findFirst().get().getValue()
															.getDefaultLocale())
													: StringPool.BLANK);
					jsonObject.put(InvestmentConstants.DIVERSIFIED_PORTFOLIO, ddlRecord
							.getDDMFormFieldValues(InvestmentConstants.DIVERSIFIED_PORTFOLIO).parallelStream().findFirst()
							.get().getValue()
							.getString(ddlRecord.getDDMFormFieldValues(InvestmentConstants.DIVERSIFIED_PORTFOLIO)
									.parallelStream().findFirst().get().getValue().getDefaultLocale()) != null ? ddlRecord
											.getDDMFormFieldValues(InvestmentConstants.DIVERSIFIED_PORTFOLIO)
											.parallelStream().findFirst().get().getValue()
											.getString(ddlRecord
													.getDDMFormFieldValues(InvestmentConstants.DIVERSIFIED_PORTFOLIO)
													.parallelStream().findFirst().get().getValue().getDefaultLocale())
											: StringPool.BLANK);
					jsonObject.put(InvestmentConstants.RISK_FACTOR,
							ddlRecord.getDDMFormFieldValues(InvestmentConstants.RISK_FACTOR).parallelStream().findFirst()
									.get().getValue()
									.getString(ddlRecord.getDDMFormFieldValues(InvestmentConstants.RISK_FACTOR)
											.parallelStream().findFirst().get().getValue().getDefaultLocale()) != null
													? ddlRecord.getDDMFormFieldValues(InvestmentConstants.RISK_FACTOR)
															.parallelStream().findFirst().get().getValue()
															.getString(ddlRecord
																	.getDDMFormFieldValues(InvestmentConstants.RISK_FACTOR)
																	.parallelStream().findFirst().get().getValue()
																	.getDefaultLocale())
													: StringPool.BLANK);
					jsonObject.put(InvestmentConstants.MIN,
							ddlRecord.getDDMFormFieldValues(InvestmentConstants.MIN).parallelStream().findFirst().get()
									.getValue()
									.getString(ddlRecord.getDDMFormFieldValues(InvestmentConstants.MIN).parallelStream()
											.findFirst().get().getValue().getDefaultLocale()) != null ? ddlRecord
													.getDDMFormFieldValues(InvestmentConstants.MIN).parallelStream()
													.findFirst().get().getValue()
													.getString(ddlRecord.getDDMFormFieldValues(InvestmentConstants.MIN)
															.parallelStream().findFirst().get().getValue()
															.getDefaultLocale())
													: StringPool.BLANK);
					jsonObject.put(InvestmentConstants.MAX,
							ddlRecord.getDDMFormFieldValues(InvestmentConstants.MAX).parallelStream().findFirst().get()
									.getValue()
									.getString(ddlRecord.getDDMFormFieldValues(InvestmentConstants.MAX).parallelStream()
											.findFirst().get().getValue().getDefaultLocale()) != null ? ddlRecord
													.getDDMFormFieldValues(InvestmentConstants.MAX).parallelStream()
													.findFirst().get().getValue()
													.getString(ddlRecord.getDDMFormFieldValues(InvestmentConstants.MAX)
															.parallelStream().findFirst().get().getValue()
															.getDefaultLocale())
													: StringPool.BLANK);
					jsonArray.put(jsonObject);
				}

			} catch (Exception e) {
				_log.error("Error while retrieving recordSet values ", e);
			}
		}
		return jsonArray;
	}

	@Override
	public double getFirstValueByKey(String ddlName, String key) {
		List<DDLRecord> records = getDDLRecords(ddlName);
		for (DDLRecord ddlRecord : records) {
			try {
				String value = ddlRecord.getDDMFormFieldValues(key).parallelStream().findFirst().get()
						.getValue()
						.getString(ddlRecord.getDDMFormFieldValues(key).parallelStream()
								.findFirst().get().getValue().getDefaultLocale());
				return !value.isEmpty() ? Double.valueOf(value) : 0;
			} catch (Exception e) {
				_log.error("Error while retrieving recordSet values, getFirstValueByKey ", e);
			}
		}
		return 0;
	}

}

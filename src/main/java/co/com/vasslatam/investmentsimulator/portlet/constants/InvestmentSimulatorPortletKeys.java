package co.com.vasslatam.investmentsimulator.portlet.constants;

/**
 * @author digberth
 */
public class InvestmentSimulatorPortletKeys {

	public static final String INVESTMENTSIMULATOR =
		"com_vasslatam_investmentsimulator_portlet_InvestmentSimulatorPortlet";
	
	public static final String PREFERENCE_MENU_URL = "urlMenu";	

}


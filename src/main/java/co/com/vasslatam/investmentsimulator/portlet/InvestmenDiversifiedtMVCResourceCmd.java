package co.com.vasslatam.investmentsimulator.portlet;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCResourceCommand;
import com.liferay.portal.kernel.util.Constants;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.porvenir.luis.portafolio.model.Diversificado;
import com.porvenir.luis.portafolio.model.Pura;
import com.porvenir.luis.portafolio.service.DiversificadoLocalServiceUtil;
import com.porvenir.luis.portafolio.service.PuraLocalServiceUtil;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.portlet.PortletException;
import javax.portlet.PortletSession;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import co.com.vasslatam.investmentsimulator.domain.FanChartScenario;
import co.com.vasslatam.investmentsimulator.domain.IDynamicDataList;
import co.com.vasslatam.investmentsimulator.domain.IFanChartsCalculations;
import co.com.vasslatam.investmentsimulator.domain.InvestmentConstants;
import co.com.vasslatam.investmentsimulator.portlet.constants.InvestmentSimulatorPortletKeys;
import co.com.vasslatam.investmentsimulator.util.DiversifiedType;

/**
 * @author digberth
 * 
 */
@Component(property = { "javax.portlet.name=" + InvestmentSimulatorPortletKeys.INVESTMENTSIMULATOR,
		"mvc.command.name=get-diversified" }, service = MVCResourceCommand.class)

public class InvestmenDiversifiedtMVCResourceCmd implements MVCResourceCommand {

	private static final Log _log = LogFactoryUtil.getLog(InvestmenDiversifiedtMVCResourceCmd.class);

	@Reference
	IDynamicDataList dynamicService;
	
	@Reference
	IFanChartsCalculations fanChartsCalcService;

	@Override
	public boolean serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse)
			throws PortletException {
		String cmd = ParamUtil.getString(resourceRequest, Constants.CMD);
		String score = ParamUtil.getString(resourceRequest, InvestmentConstants.SCORE, "0");
		String target = ParamUtil.getString(resourceRequest, InvestmentConstants.TARGET);		
		double begin = ParamUtil.getDouble(resourceRequest, InvestmentConstants.INITIAL_INVESTMENT);
		double save = ParamUtil.getDouble(resourceRequest, InvestmentConstants.MONTHLY_SAVINGS);
		double income = ParamUtil.getDouble(resourceRequest, InvestmentConstants.EXPECTED_INCOME, 0);
		int years = ParamUtil.getInteger(resourceRequest, InvestmentConstants.TIME, 0);		
		
		try {
			int intScore = Integer.valueOf(score);
			double inflation = dynamicService.getFirstValueByKey(InvestmentConstants.DDL_NAME_INFLATION, InvestmentConstants.INFLATION_KEY);
						
			if (cmd.toLowerCase().equals(InvestmentConstants.DIVERSIFIED.toLowerCase())) {
				JSONObject diversifiedValues = JSONFactoryUtil.createJSONObject();
				
				List<Diversificado> diversificados = DiversificadoLocalServiceUtil.getByScore(intScore);
				JSONArray diversifiedJSON = diversifiedToJSONArray(diversificados);

				if (!diversificados.isEmpty()) {
					Diversificado diversificado = diversificados.get(0);
					String diversifiedType = diversificado.getNombre();

					int id = DiversifiedType.getByValue(diversifiedType).getNumValue();
					JSONArray inversionRangeJSON = dynamicService.filterByKeyValue(InvestmentConstants.DDL_NAME_DIVERSIFICADOS,
							InvestmentConstants.ID, String.valueOf(id));
							
					List<FanChartScenario> data = getDiversifiedChartData( target, years,
					diversifiedType, begin, inflation, save*12, income*12);
					
					double projProfitable = fanChartsCalcService.getProjectedProfitability(diversifiedType, 0, years);
					
				    JSONArray chartData = chartToJSONArray(data);
					diversifiedValues.put("alternative", diversifiedJSON);
					diversifiedValues.put("inversion", inversionRangeJSON);
					diversifiedValues.put("chart", chartData);
					diversifiedValues.put("rent", projProfitable);
				}	
				
				resourceResponse.getWriter().print(diversifiedValues);
			} else if (cmd.toLowerCase().equals(InvestmentConstants.PURE.toLowerCase())) {
				JSONObject pureValues = JSONFactoryUtil.createJSONObject();
				
				List<Pura> puras = PuraLocalServiceUtil.getByScore(intScore);
				JSONArray pureJSON = puresToJSONArray(puras);
				
				List<FanChartScenario> data = getPureChartData( target, years,
						intScore, begin, inflation, save*12, income*12);
				
				double projProfitable = fanChartsCalcService.getProjectedProfitability(StringPool.BLANK, intScore, years);
				
			    JSONArray chartData = chartToJSONArray(data);	
			    pureValues.put("alternative", pureJSON);
			    pureValues.put("chart", chartData);
			    pureValues.put("rent", projProfitable);
			    
				resourceResponse.getWriter().print(pureValues);
			}else if(cmd.toLowerCase().equals(InvestmentConstants.SESSION_SAVE_INVESTMENT.toLowerCase())) {
				String userName = ParamUtil.getString(resourceRequest, "userName");		
				resourceRequest.getPortletSession(true).setAttribute("LIFERAY_SHARED_personName", userName, PortletSession.APPLICATION_SCOPE);
				resourceRequest.getPortletSession(true).setAttribute("LIFERAY_SHARED_invCompleted", "active", PortletSession.APPLICATION_SCOPE);
			}else if(cmd.toLowerCase().equals(InvestmentConstants.SESSION_CLEAR.toLowerCase())) {
				resourceRequest.getPortletSession().removeAttribute("LIFERAY_SHARED_invCompleted", PortletSession.APPLICATION_SCOPE);
				resourceRequest.getPortletSession().removeAttribute("LIFERAY_SHARED_personName", PortletSession.APPLICATION_SCOPE);
			}
			
		} catch (IOException e) {
			_log.error("Error in InvestmenDiversifiedtMVCResourceCmd:serveResource ", e);
		}
		return true;
	}

	private JSONArray diversifiedToJSONArray(List<Diversificado> diversificados) {
		JSONArray jsonArray = JSONFactoryUtil.createJSONArray();
		for (Diversificado diversificado : diversificados) {
				JSONObject jsonObject = JSONFactoryUtil.createJSONObject();

				jsonObject.put("score", diversificado.getScore());
				jsonObject.put("nombre", diversificado.getNombre());
				jsonObject.put("suma_peso", diversificado.getSuma_peso());
				jsonObject.put("factor_riesgo", diversificado.getFactor_riesgo());
				jsonObject.put("descobertura", diversificado.getDescobertura());

				jsonArray.put(jsonObject);			
		}

		return jsonArray;
	}

	private JSONArray puresToJSONArray(List<Pura> puras) {
		JSONArray jsonArray = JSONFactoryUtil.createJSONArray();
		for (Pura pura : puras) {
				JSONObject jsonObject = JSONFactoryUtil.createJSONObject();

				jsonObject.put("score", pura.getScore());
				jsonObject.put("nombre", pura.getNombre());
				jsonObject.put("peso", pura.getPeso());
				jsonObject.put("alternativa", pura.getAlternativa());

				jsonArray.put(jsonObject);			
		}

		return jsonArray;
	}
	
	private JSONArray chartToJSONArray(List<FanChartScenario> list) {
		JSONArray chartArray = JSONFactoryUtil.createJSONArray();		  
		  for (FanChartScenario obj : list) { 
			  JSONObject jsonObj = JSONFactoryUtil.createJSONObject();
			  
			  jsonObj.put("negative" , obj.getNegative());
			  jsonObj.put("probable" , obj.getProbable());
			  jsonObj.put("positive" , obj.getPositive());
			  chartArray.put(jsonObj);
		  }
		  
		  return chartArray;
	}

	private List<FanChartScenario> getDiversifiedChartData(String savingReason, int years_RetireOrInvestment, String diversifiedPortfolio, 
			double startingAmount, double inflation, double monthlySavingsAmount, double expectedAnnualIncome) {
		
		List<FanChartScenario> data = new ArrayList<FanChartScenario>();
		data = fanChartsCalcService.getChartData(diversifiedPortfolio, 0, startingAmount, years_RetireOrInvestment, monthlySavingsAmount, expectedAnnualIncome, savingReason, inflation);
		
		return data;
	}
	
	private List<FanChartScenario> getPureChartData(String savingReason, int years_RetireOrInvestment, int score, 
			double startingAmount, double inflation, double monthlySavingsAmount, double expectedAnnualIncome) {
		
		List<FanChartScenario> data = new ArrayList<FanChartScenario>();
		data = fanChartsCalcService.getChartData(StringPool.BLANK, score, startingAmount, years_RetireOrInvestment, monthlySavingsAmount, expectedAnnualIncome, savingReason, inflation);
		
		return data;
	}

}

package co.com.vasslatam.investmentsimulator.portlet;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;

import javax.portlet.Portlet;

import org.osgi.service.component.annotations.Component;

import co.com.vasslatam.investmentsimulator.portlet.constants.InvestmentSimulatorPortletKeys;

/**
 * @author digberth
 */
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=category.luis.porvenir",
		"com.liferay.portlet.header-portlet-css=/css/main.css",
		"com.liferay.portlet.header-portlet-css=/css/datepicker.css",
		"com.liferay.portlet.header-portlet-css=/css/slick.css",
		"com.liferay.portlet.header-portlet-css=/css/simulator.css",
		"com.liferay.portlet.header-portlet-javascript=/js/canvasjs.min.js",
		"com.liferay.portlet.header-portlet-javascript=/js/bootstrap-datepicker.js",
		"com.liferay.portlet.header-portlet-javascript=/js/vanilla-masker.min.js",
		"com.liferay.portlet.header-portlet-javascript=/js/slick.min.js",
		"com.liferay.portlet.header-portlet-javascript=/js/jquery.validate.js",
		"com.liferay.portlet.header-portlet-javascript=/js/jquery.validate.min.js",
		"com.liferay.portlet.footer-portlet-javascript=/js/simulator.js",
		"com.liferay.portlet.instanceable=false",
		"javax.portlet.display-name=InvestmentSimulator",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + InvestmentSimulatorPortletKeys.INVESTMENTSIMULATOR,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user"
	},
	service = Portlet.class
)
public class InvestmentSimulatorPortlet extends MVCPortlet {
	
}
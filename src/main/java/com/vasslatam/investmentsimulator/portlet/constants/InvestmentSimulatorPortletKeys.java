package com.vasslatam.investmentsimulator.portlet.constants;

/**
 * @author digberth
 */
public class InvestmentSimulatorPortletKeys {

	public static final String INVESTMENTSIMULATOR =
		"com_vasslatam_investmentsimulator_portlet_InvestmentSimulatorPortlet";

}